package net.luckyverse.sugarboy;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class Logger {

    private JavaPlugin plugin;
    private ConsoleCommandSender console;

    public Logger(JavaPlugin plugin) {
        this.plugin = plugin;
        this.console = Bukkit.getConsoleSender();
    }

    public void info(Class clazz, String message) {
        console.sendMessage(String.format("§9[%s] %s: §7%s", plugin.getName(), clazz.getSimpleName(), message));
    }

    public void warn(Class clazz, String message) {
        console.sendMessage(String.format("§9[%s] %s: §c%s", plugin.getName(), clazz.getSimpleName(), message));
    }

    public void error(Class clazz, String message) {
        console.sendMessage(String.format("§9[%s] %s: §4%s", plugin.getName(), clazz.getSimpleName(), message));
    }

    public void debug(Class clazz, String message) {
        console.sendMessage(String.format("§9[%s] %s: §c%s", plugin.getName(), clazz.getSimpleName(), message));
    }
}
