package net.luckyverse.sugarboy.inventories;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import java.util.HashSet;
import java.util.Set;

public class MyInventoryListener implements Listener {

    public Set<MyInventory> inventories = new HashSet<>();

    public void registerInventory(MyInventory inventory) {
        inventories.add(inventory);
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Inventory inv = e.getInventory();

        MyInventory myInventory = inventories.stream()
                .filter(i -> i.getInventory().equals(inv))
                .findFirst()
                .orElse(null);

        if (myInventory != null && e.getCurrentItem() != null) {
            e.setCancelled(true);
            Runnable runnable = myInventory.getRunnable(e.getSlot());
            if (runnable != null)
                runnable.run();
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        //Player player = (Player) e.getPlayer();
        Inventory inv = e.getInventory();

        MyInventory myInventory = inventories.stream()
                .filter(i -> i.getInventory().equals(inv))
                .findFirst()
                .orElse(null);

        if (myInventory != null) {
            inventories.remove(myInventory);
            myInventory.eventClose();
        }
    }
}
