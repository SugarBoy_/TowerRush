package net.luckyverse.sugarboy.inventories;

import net.luckyverse.sugarboy.SugarAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class MyInventory {

    private MyInventoryListener listener;

    protected Map<Integer, Runnable> runnableMap = new HashMap<>();
    protected Inventory inventory;
    protected Player player;

    public MyInventory(Player player, int rows, String name) {
        if (listener == null)
            Bukkit.getPluginManager().registerEvents(listener = new MyInventoryListener(), SugarAPI.getApi());

        listener.registerInventory(this);
        inventory = Bukkit.createInventory(null, 9 * rows, name);
        this.player = player;

        eventOpen();
        player.openInventory(inventory);
    }

    public abstract void eventOpen();

    public abstract void eventClose();

    public void setItem(int slot, ItemStack item) {
        this.setItem(slot, item, null);
    }

    public void setItem(int slot, ItemStack item, Runnable runnable) {
        inventory.setItem(slot, item);
        if (runnable != null)
            runnableMap.put(slot, runnable);
    }

    public Inventory getInventory() {
        return inventory;
    }

    public Player getPlayer() {
        return player;
    }

    public Runnable getRunnable(int slot) {
        return runnableMap.get(slot);
    }

    protected ItemStack item(ItemStack item, String name, List<String> lore) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }
}
