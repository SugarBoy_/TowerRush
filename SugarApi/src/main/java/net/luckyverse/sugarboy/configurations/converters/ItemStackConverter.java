package net.luckyverse.sugarboy.configurations.converters;

import com.google.gson.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ItemStackConverter implements JsonSerializer<ItemStack>, JsonDeserializer<ItemStack> {

    @Override
    public ItemStack deserialize(JsonElement element, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

        JsonObject json = element.getAsJsonObject();
        int itemId = json.get("id").getAsInt();
        int itemData = json.get("data").getAsInt();
        int itemCount = json.get("count").getAsInt();
        ItemStack item = new ItemStack(itemId, itemCount, (short) itemData);
        ItemMeta meta = item.getItemMeta();
        if (json.has("name"))
            meta.setDisplayName(json.get("name").getAsString());

        if (json.has("lore")) {
            JsonArray array = json.get("lore").getAsJsonArray();
            List<String> lore = new ArrayList<>();
            for (JsonElement line : array)
                lore.add(line.getAsString().replace("&", "§"));
            meta.setLore(lore);
        }

        if (json.has("enchantments")) {
            JsonObject enchantments = json.get("enchantments").getAsJsonObject();
            for (Map.Entry<String, JsonElement> entry : enchantments.entrySet()) {
                String enchantdisplayName = entry.getKey();
                Enchantment enchantment = Enchantment.getByName(enchantdisplayName);
                int level = entry.getValue().getAsInt();
                meta.addEnchant(enchantment, level, true);
            }
        }

        item.setItemMeta(meta);
        return item;
    }

    @Override
    public JsonElement serialize(ItemStack item, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject element = new JsonObject();
        element.addProperty("id", item.getTypeId());
        element.addProperty("data", item.getDurability());
        element.addProperty("count", item.getAmount());

        ItemMeta meta = item.getItemMeta();
        if (meta.hasDisplayName())
            element.addProperty("name", meta.getDisplayName());

        if (meta.hasLore()) {
            JsonArray lore = new JsonArray();
            meta.getLore().forEach(line -> lore.add(line));
            element.add("lore", lore);
        }

        if (meta.hasEnchants()) {
            JsonObject enchants = new JsonObject();
            for (Map.Entry<Enchantment, Integer> entry : item.getEnchantments().entrySet())
                enchants.addProperty(entry.getKey().getName(), entry.getValue());
            element.add("enchantments", enchants);
        }

        return element;
    }
}