package net.luckyverse.sugarboy.configurations.converters;

import com.google.gson.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.lang.reflect.Type;

public class LocationConverter implements JsonSerializer<Location>, JsonDeserializer<Location> {


    @Override
    public JsonElement serialize(Location location, Type type, JsonSerializationContext jsonSerializationContext) {
        String json = String.format("%s %d %d %d %.2f %.2f",
                location.getWorld().getName(),
                location.getBlockX(),
                location.getBlockY(),
                location.getBlockZ(),
                location.getYaw(),
                location.getPitch());
        return new JsonPrimitive(json);
    }

    @Override
    public Location deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonPrimitive primitive = jsonElement.getAsJsonPrimitive();
        String json = primitive.getAsString();
        String[] data = json.split(" ");

        int x = Integer.valueOf(data[1]);
        int z = Integer.valueOf(data[3]);



        return data.length > 4 ?

                new Location(
                        Bukkit.getWorld(data[0]),
                        x < 0 ? x - 0.5 : x + 0.5,
                        Integer.valueOf(data[2]),
                        z < 0 ? z - 0.5 : z + 0.5,
                        Float.valueOf(data[4]),
                        Float.valueOf(data[5]))

                :

                new Location(
                        Bukkit.getWorld(data[0]),
                        x < 0 ? x - 0.5 : x + 0.5,
                        Integer.valueOf(data[2]),
                        z < 0 ? z - 0.5 : z + 0.5);
    }
}
