package net.luckyverse.sugarboy.configurations;

import com.google.gson.reflect.TypeToken;

public interface Configuration<T> {

    /**
     * @return Вернуть лист предметов класс T
     * */
    T read();

    /**
     * Загрузить конфиг
     *
     * @param dir - директория конфигурации.
     * @param file - имя файла.
     * @param typeToken - тип объекта выхода
     */
    Configuration<T> load(String dir, String file, TypeToken typeToken);
}
