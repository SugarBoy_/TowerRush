package net.luckyverse.sugarboy;

import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

public class StringMetadata implements MetadataValue {

    private String s;

    public StringMetadata(String s) {
        this.s = s;
    }

    public StringMetadata(int s) {
        this.s = s + "";
    }

    @Override
    public Object value() {
        return s;
    }

    @Override
    public int asInt() {
        return Integer.valueOf(s);
    }

    @Override
    public float asFloat() {
        return 0;
    }

    @Override
    public double asDouble() {
        return 0;
    }

    @Override
    public long asLong() {
        return 0;
    }

    @Override
    public short asShort() {
        return 0;
    }

    @Override
    public byte asByte() {
        return 0;
    }

    @Override
    public boolean asBoolean() {
        return false;
    }

    @Override
    public String asString() {
        return s;
    }

    @Override
    public Plugin getOwningPlugin() {
        return SugarAPI.getApi();
    }

    @Override
    public void invalidate() {

    }
}
