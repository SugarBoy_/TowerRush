package net.luckyverse.sugarboy.files;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.luckyverse.sugarboy.SugarAPI;
import net.luckyverse.sugarboy.configurations.converters.ItemStackConverter;
import net.luckyverse.sugarboy.configurations.converters.LocationConverter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.nio.file.Files;

public class MyFileUtils {

    public static File autoCreate(String source, String path) {
        InputStream copyStream = SugarAPI.getApi().getResource(source);
        File config = new File(path);
        if (!config.exists()) {
            try {
                Files.copy(copyStream, config.toPath());

                SugarAPI.getApi().getLogger().info(
                        String.format("Конфигурация %s была создана, так как ее не было.", config.getName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return config;
    }

    public static boolean copySchemWorld(JavaPlugin plugin, String source, String world) {
        File schemWorld = new File(Bukkit.getWorldContainer().getPath() +
                "/plugins/" + plugin.getName() + "/" + source + "/" + world);
        File oldWorld = new File(Bukkit.getWorldContainer().getPath() + "/" + world);

        if (!schemWorld.exists()) {
            SugarAPI.getApi().getLogger().warning(
                    String.format(String.format("Мир %s не был скопирован, так как исходный мир не был найден.", world)));
            return false;
        }

        if (oldWorld.exists()) {
            if (delete(oldWorld)) {
                SugarAPI.getApi().getLogger().info(
                        String.format(String.format("Мир %s был успешно удален.", world)));
            } else {
                SugarAPI.getApi().getLogger().info(
                        String.format(String.format("Мир %s не был удален.", world)));
                return false;
            }
        }

        try {
            copy(schemWorld, oldWorld);
            SugarAPI.getApi().getLogger().info(
                            String.format(String.format("Мир %s был успешно скопирован.", world)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static void printJsonFormat(Object object) {
        Gson gson = new GsonBuilder()
                //.setPrettyPrinting()
                .registerTypeAdapter(ItemStack.class, new ItemStackConverter())
                .registerTypeAdapter(Location.class, new LocationConverter())
                .create();
        String json = gson.toJson(object);
        System.out.println(json);
    }

    public static void copy(File sourceLocation, File targetLocation) throws IOException {
        if (sourceLocation.isDirectory()) {
            copyDirectory(sourceLocation, targetLocation);
        } else {
            copyFile(sourceLocation, targetLocation);
        }
    }

    private static void copyDirectory(File source, File target) throws IOException {
        if (!target.exists()) {
            target.mkdir();
        }

        for (String f : source.list()) {
            copy(new File(source, f), new File(target, f));
        }
    }

    private static void copyFile(File source, File target) throws IOException {
        try (
                InputStream in = new FileInputStream(source);
                OutputStream out = new FileOutputStream(target)
        ) {
            byte[] buf = new byte[1024];
            int length;
            while ((length = in.read(buf)) > 0) {
                out.write(buf, 0, length);
            }
        }
    }

    private static boolean delete(File f) {
        if (f.isDirectory()) {
            for (File c : f.listFiles())
                delete(c);
        }
        return f.delete();
    }
}
