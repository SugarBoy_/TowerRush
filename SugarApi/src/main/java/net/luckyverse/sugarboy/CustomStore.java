package net.luckyverse.sugarboy;

import java.util.HashMap;
import java.util.Map;

public abstract class CustomStore<Key, Value> {

    protected Map<Key, Value> map = new HashMap<>();

    public void add(Key key, Value value) {
        map.put(key, value);
    }

    public void remove(Key key) {
        map.remove(key);
    }

    public boolean containsKey(Key key) {
        return map.containsKey(key);
    }

    public boolean containsValue(Value value) {
        for (Value v : map.values()) {
            if (v.equals(value))
                return true;
        }
        return false;
    }

    public Value get(Key key) {
        return map.get(key);
    }

    public Map<Key, Value> map() {
        return map;
    }

    public int size() {
        return map.size();
    }
}
