package net.luckyverse.sugarboy.cosmetics;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Holograms {

    private static Map<String, List<ArmorStand>> map = new HashMap<String, List<ArmorStand>>();

    public static void setText(String name, Location l, String... text) {
        List<ArmorStand> list = new ArrayList<ArmorStand>();
        int size = text.length;
        int y = 0;
        for (float q = 0.25F * size; q > 0; q -= 0.25F) {
            list.add(create(text[y], l.clone().add(0, q, 0)));
            y++;
        }
        map.put(name, list);
    }

    private static ArmorStand create(String text, Location location) {
        ArmorStand am = location.getWorld().spawn(location, ArmorStand.class);
        am.setVisible(false);
        am.setCustomNameVisible(true);
        am.setGravity(false);
        am.setCanPickupItems(true);
        am.setCustomName(text);
        return am;
    }

    public static boolean removeHologram(String name) {
        if (map.containsKey(name)) {
            for (ArmorStand am : map.get(name)) am.remove();
            map.remove(name);
            return true;
        } else return false;
    }

    public static boolean editText(String name, int line, String text) {
        if (map.containsKey(name)) {
            map.get(name).get(--line).setCustomName(text);
            return true;
        } else return false;
    }

    public static boolean removeLine(String name, int line) {
        if (map.containsKey(name)) {
            map.get(name).get(--line).remove();
            return true;
        } else return false;
    }

}
