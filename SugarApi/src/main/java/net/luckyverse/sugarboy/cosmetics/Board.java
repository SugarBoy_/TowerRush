package net.luckyverse.sugarboy.cosmetics;

import net.luckyverse.sugarboy.SugarAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Board {

    private ScoreboardManager manager;
    private Scoreboard gameScore;
    private Objective game;
    private Map<Integer, Team> list = new HashMap<>();

    private static int a = 1;

    public Board(String name) {
        manager = Bukkit.getScoreboardManager();
        gameScore = manager.getNewScoreboard();
        if (game != null) game.unregister();
        game = gameScore.registerNewObjective("board: " + a++, "dummy");
        game.setDisplaySlot(DisplaySlot.SIDEBAR);
        game.setDisplayName(name);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(SugarAPI.getApi(), ()-> uppdate(), 0L, 20L);
    }

    public void uppdate() {

    }

    public void updateSuffix(int line, String s) {
        Team t = list.get(line);
        t.setSuffix(s);
    }

    public void updatePrefix(int line, String s) {
        Team t = list.get(line);
        t.setPrefix(s);
    }

    public Scoreboard getScoreboard() {
        return gameScore;
    }

    public Team setLine(int line, String s) {

        String prefix = "";
        String name = "";
        String suffix = "";

        char[] chars = s.toCharArray();

        int q = 0;
        for (char c : chars) {
            if (q < 16)
                prefix += c;
            else if (q < 32)
                name += c;
            else if (q < 48)
                suffix += c;
            else
                continue;
            q++;
        }

        if (name == "") {
            name = "";
            for (int i = 0; i < line; i++) name += "";
        }

        Team team = gameScore.getTeam("line_" + line);
        if (team == null) team = gameScore.registerNewTeam("line_" + line);
        gameScore.getTeam("line_" + line);
        team.addEntry(name);
        team.setPrefix(prefix);
        team.setSuffix(suffix);

        game.getScore(name).setScore(line);
        gameScore.getTeam("line_" + line);

        list.put(line, team);

        return team;
    }

    public void setName(String s) {
        game.setDisplayName(s);
    }

    public void setAll() {
        for (Player p : Bukkit.getOnlinePlayers()) p.setScoreboard(this.gameScore);
    }

    public void setForAll(Collection<Player> list) {
        for (Player p : list) p.setScoreboard(this.gameScore);
    }
}
