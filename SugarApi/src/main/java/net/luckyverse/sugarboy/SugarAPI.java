package net.luckyverse.sugarboy;

import org.bukkit.plugin.java.JavaPlugin;

public class SugarAPI extends JavaPlugin {

    private static SugarAPI api;

    public void onEnable() {
        api = this;
    }

    public static SugarAPI getApi() {
        return api;
    }
}
