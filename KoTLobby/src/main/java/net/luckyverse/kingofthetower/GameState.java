package net.luckyverse.kingofthetower;

public enum GameState {

    WAITING(0),
    STARTING(1),
    PLAYING(2),
    ENDING(3),
    RESTARTING(4),
    STOP(5);

    private int number;

    GameState(int number) {
        this.number = number;
    }

    public int number() {
        return number;
    }

    public static GameState get(int id) {
        for (GameState state : values())
            if (state.number == id)
                return state;
        return STOP;
    }
}
