package net.luckyverse.kingofthetower.listeners;

import net.luckyverse.kingofthetower.KTLobby;
import net.luckyverse.sugarboy.cosmetics.Board;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.inventory.ItemStack;
import ru.yooxa.connecter.Core;

import java.util.concurrent.ThreadLocalRandom;

public class LobbyListener implements Listener {

    private Board board;

    public LobbyListener() {
        board = new Board("§c§lKing of the Tower");
        board.setLine(16, "§7");
        board.setLine(15, "§7В данный момент режим");
        board.setLine(14, "§7находится на этапе");
        board.setLine(13, "§7тестирования, поэтому");
        board.setLine(12, "§7приносим свои извенения");
        board.setLine(11, "§7за какие-либо неполадки.");
        board.setLine(10, "§7");
        board.setLine(9, "§7О идеях и багах писать ему:");
        board.setLine(8, "§avk.com/sugarisboy");

        Bukkit.getScheduler().runTaskTimer(KTLobby.plugin(), ()-> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.stopSound(Sound.BLOCK_PORTAL_TRAVEL);
                player.stopSound(Sound.BLOCK_PORTAL_AMBIENT);
                player.stopSound(Sound.BLOCK_PORTAL_TRIGGER);
            }
        }, 0L, 1L);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.getPlayer().setScoreboard(board.getScoreboard());
    }

    @EventHandler
    public void onPortal(PlayerPortalEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onCliCkChest(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            Player player = e.getPlayer();
            ItemStack item = player.getInventory().getItemInMainHand();
            if (item != null && item.getType().equals(Material.BED)) {
                Core.redirect(player, String.format("Lobby-%d", ThreadLocalRandom.current().nextInt(2) + 1));
                return;
            }
        }

        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (e.getClickedBlock().getType().equals(Material.ENDER_CHEST)) {
                e.setCancelled(true);

                Player player = e.getPlayer();
                player.sendMessage(" §cПока недоступно :c");
            }
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onHungry(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (e.getTo().getY() < 10) {
            e.getPlayer().performCommand("spawn");
        }

        World world = e.getTo().getWorld();
        Block block = world.getBlockAt(e.getTo());
        if (block.getType().equals(Material.PORTAL)) {
            e.getPlayer().performCommand("game");
        }
    }
}
