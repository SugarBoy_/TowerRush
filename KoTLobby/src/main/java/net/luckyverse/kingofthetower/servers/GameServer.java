package net.luckyverse.kingofthetower.servers;

import com.google.gson.annotations.Expose;

import java.util.List;

public class GameServer implements Comparable<GameServer> {

    @Expose
    private String server;

    @Expose
    private List<GameArena> arenas;

    private long lastUpdateTime;

    public void updateTime() {
        this.lastUpdateTime = System.currentTimeMillis();
    }

    public int timeBeforeUpdate() {
        return (int) ((System.currentTimeMillis() - lastUpdateTime) / 1000);
    }

    public String getServerName() {
        return server;
    }

    public List<GameArena> getArenas() {
        return arenas;
    }

    public GameArena getArena(String name) {
        for (GameArena arena : arenas)
            if (arena.getName().equals(name))
                return arena;
        return null;
    }

    public int compareTo(GameServer o) {
        return -(int) o.getArenas().stream()
                .filter(a -> !(a.getState().equals(GameState.STARTING) || a.getState().equals(GameState.WAITING)))
                .count();
    }
}
