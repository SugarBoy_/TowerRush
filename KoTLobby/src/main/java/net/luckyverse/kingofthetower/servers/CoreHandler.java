package net.luckyverse.kingofthetower.servers;

import com.google.gson.Gson;
import net.luckyverse.kingofthetower.KTLobby;
import net.luckyverse.kingofthetower.utils.Pair;
import net.luckyverse.kingofthetower.profiles.ProfileSettings;
import net.luckyverse.sugarboy.Logger;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import ru.yooxa.connecter.Core;
import ru.yooxa.connecter.events.CoreMessageEvent;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class CoreHandler implements Listener {

    private final Set<Player> redirects = new HashSet<>();

    public boolean isLog = false;

    private Pair<GameServer, GameArena> optimal = new Pair<>();

    private Map<String, GameServer> store = new HashMap<>();
    private Gson gson = new Gson();
    private Logger log;

    public CoreHandler(Logger log) {
        this.log = log;
        KTLobby plugin = KTLobby.plugin();

        Bukkit.getPluginManager().registerEvents(this, plugin);
        Bukkit.getScheduler().runTaskTimer(plugin, ()-> autoClearStoped(), 500L, 250L);
    }

    public void autoClearStoped() {
        Iterator<Map.Entry<String, GameServer>> iter = store.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, GameServer> entry = iter.next();
            //String name = entry.getKey();
            GameServer server = entry.getValue();

            if (server.timeBeforeUpdate() >= 12) {

                if (isLog)
                    log.info(CoreHandler.class, String.format("Сервер %s был удален из списка, так как долго не выходил на связь.",
                            server.getServerName()));

                iter.remove();
            }
        }
    }

    public void updateInfo(String json) {
        try {
            GameServer server = gson.fromJson(json, GameServer.class);
            store.put(server.getServerName(), server);

            if (!optimal.isClear() && optimal.getA().getServerName().equals(server.getServerName())) {
                optimal.set(server, server.getArena(optimal.getB().getName()));
            }

            server.updateTime();

            if (isLog)
                log.info(CoreHandler.class, String.format("Сервер %s успешно обновил информацию.", server.getServerName()));
        } catch (Exception ex) {
            log.info(CoreHandler.class, "Сервер не смог обновить информацию:");
            ex.printStackTrace();
        }
    }

    public void updatePlayers(String msg) {
        String[] data = msg.split(" ");

        GameServer server = store.get(data[0]);
        if (server == null) {
            log.warn(CoreHandler.class, String.format("Сервер %s не был найден.", data[0]));
            return;
        }

        GameArena arena = server.getArena(data[1]);
        if (arena == null) {
            log.warn(CoreHandler.class, String.format("Арена %s - %s не была найден.", data[0], data[1]));
            return;
        }

        arena.setPlayers(Integer.valueOf(data[2]));
    }

    public void updateState(String msg) {
        String[] data = msg.split(" ");

        GameServer server = store.get(data[0]);
        if (server == null) {
            log.warn(CoreHandler.class, String.format("Сервер %s не был найден.", data[0]));
            return;
        }

        GameArena arena = server.getArena(data[1]);
        if (arena == null) {
            log.warn(CoreHandler.class, String.format("Арена %s - %s не была найден.", data[0], data[1]));
            return;
        }

        arena.setState(Integer.valueOf(data[2]));
        if (isLog) {
            log.info(CoreHandler.class, String.format("Арена %s-%s теперь в состоянии %s.", server.getServerName(),
                    arena.getName(), arena.getState().name()));
        }
    }

    public void sendToRandomServer(Player player, ProfileSettings settings) {
        if (redirects.contains(player)) {
            return;
        }

        player.sendMessage(" §aНачинаем поиск подходящей арены..");

        if (optimal.isClear() || !optimal.getB().isCanJoin())
            findOptimal();

        GameServer server = optimal.getA();
        GameArena arena = optimal.getB();

        if (server == null) {
            player.sendMessage(" §cАрены не были найдены, повторите попытку позже.");
            return;
        }

        redirectToArena(player, server.getServerName(), arena.getName());
        arena.setPlayers(arena.getPlayers() + 1);
    }

    public void redirectToArena(Player player, String server, String arena) {
        redirects.add(player);
        try {
            Core.sendMessageToCore(server, "changearena", arena + " " + player.getName());

            if (isLog)
                log.info(CoreHandler.class, String.format("[%s] %s -> %s", server, player.getName(), server));

            Bukkit.getScheduler().runTaskLater(KTLobby.plugin(), ()-> {
                redirects.remove(player);
                Core.redirect(player, server);
            }, 20L);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private GameServer findOptimal() {
        for (GameServer server : getServers()) {
            for (GameArena arena : server.getArenas()) {
                if (!arena.isCanJoin())
                    continue;

                optimal.set(server, arena);
                if (isLog)
                    log.info(CoreHandler.class, String.format("Оптимальная арена теперь: %s -> %s",
                            server.getServerName(), arena.getName()));
                return server;
            }
        }
        return null;
    }

    public Collection<GameServer> getServers() {
        List<GameServer> list = store.values().stream().collect(Collectors.toList());
        Collections.sort(list);
        return list;
    }

    @EventHandler
    public void onCoreMessage(CoreMessageEvent e) {
        if (e.getTag().equalsIgnoreCase("info")) {
            updateInfo(e.getMessage());
        } else if (e.getTag().equalsIgnoreCase("changeplayers")) {
            updatePlayers(e.getMessage());
        } else if (e.getTag().equalsIgnoreCase("changestate")) {
            updateState(e.getMessage());
        }

        /*if (isLog && e.getSender().contains("KoT")) {
            System.out.println(e.getSender());
            System.out.println(e.getTag());
            System.out.println(e.getMessage());
        }*/
    }
}
