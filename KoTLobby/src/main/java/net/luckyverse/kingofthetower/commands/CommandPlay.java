package net.luckyverse.kingofthetower.commands;

import net.luckyverse.kingofthetower.KTLobby;
import net.luckyverse.kingofthetower.servers.CoreHandler;
import net.luckyverse.kingofthetower.servers.GameArena;
import net.luckyverse.kingofthetower.servers.GameServer;
import net.luckyverse.sugarboy.files.MyFileUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.yooxa.connecter.Core;

public class CommandPlay implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (sender instanceof Player) {

            if (args.length == 0) {
                Player player = (Player) sender;
                if (player.hasPermission("kot.join")) {
                    KTLobby.plugin().getCoreHandler().sendToRandomServer(player, null);
                } else {
                    player.sendMessage("");
                    player.sendMessage(" §aДля игры на данном режиме необходимо иметь любой донат.");
                    player.sendMessage(" §7Ограничение установлено на время тестирования режима.");
                    player.sendMessage("");
                    player.sendMessage(" §7Купить донат можно тут: §aluckyverse.com");
                    player.sendMessage("");
                }
            }
        } else {
            CoreHandler coreHandler = KTLobby.plugin().getCoreHandler();
            if (args.length == 0) {
                MyFileUtils.printJsonFormat(coreHandler.getServers());

                for (GameServer server : coreHandler.getServers()) {
                    KTLobby.logger().info(Core.class, String.format("%s:", server.getServerName()));
                    for (GameArena arena : server.getArenas()) {
                        KTLobby.logger().info(Core.class, String.format(" - %s: state: %s players:%d/%d time: %d ",
                                arena.getName(), arena.getState(), arena.getPlayers(), arena.getLimit(), arena.getTime()));
                    }
                }


            } else if (args[0].equalsIgnoreCase("log")) {
                coreHandler.isLog = !coreHandler.isLog;
            }
        }
        return true;
    }
}
