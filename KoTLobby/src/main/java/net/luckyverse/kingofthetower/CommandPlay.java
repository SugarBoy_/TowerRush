package net.luckyverse.kingofthetower;

import net.luckyverse.sugarboy.files.MyFileUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandPlay implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            KTLobby.plugin().getCoreHandler().sendToRandomServer(player, null);
        } else {
            CoreHandler coreHandler = KTLobby.plugin().getCoreHandler();
            if (args.length == 0) {
                MyFileUtils.printJsonFormat(coreHandler.getServers());
            } else if (args[0].equalsIgnoreCase("log")) {
                coreHandler.isLog = !coreHandler.isLog;
            }
        }
        return true;
    }
}
