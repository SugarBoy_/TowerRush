package net.luckyverse.kingofthetower;

import com.google.gson.annotations.Expose;

public class GameArena {

    @Expose
    private String name;

    @Expose
    private int state;

    @Expose
    private int players;

    @Expose
    private int limit;

    @Expose
    private int time;

    public String getName() {
        return name;
    }

    public GameState getState() {
        return GameState.get(state);
    }

    public int getPlayers() {
        return players;
    }

    public int getLimit() {
        return limit;
    }

    public int getTime() {
        return time;
    }

    public boolean isCanJoin() {
        return time < -5 && players < limit && state <= 1;
    }

    public void setPlayers(int players) {
        this.players = players;
    }

    public void setState(int state) {
        this.state = state;
    }
}
