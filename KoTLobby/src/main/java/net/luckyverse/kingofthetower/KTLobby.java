package net.luckyverse.kingofthetower;

import net.luckyverse.kingofthetower.commands.CommandPlay;
import net.luckyverse.kingofthetower.listeners.LobbyListener;
import net.luckyverse.kingofthetower.servers.CoreHandler;
import net.luckyverse.sugarboy.Logger;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class KTLobby extends JavaPlugin {

    private static KTLobby instance;
    private static Logger logger;
    private CoreHandler coreHandler;

    public void onEnable() {
        instance = this;
        logger = new Logger(this);
        coreHandler = new CoreHandler(logger);

        Bukkit.getPluginCommand("game").setExecutor(new CommandPlay());

        Bukkit.getPluginManager().registerEvents(new LobbyListener(), this);
    }

    public static KTLobby plugin() {
        return instance;
    }

    public static Logger logger() {
        return logger;
    }

    public CoreHandler getCoreHandler() {
        return coreHandler;
    }
}
