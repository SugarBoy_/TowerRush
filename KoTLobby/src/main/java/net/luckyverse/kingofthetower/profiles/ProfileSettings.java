package net.luckyverse.kingofthetower.profiles;

import com.google.gson.annotations.Expose;

import java.util.List;

public class ProfileSettings {

    @Expose
    private boolean useSmartSearch = true;

    @Expose
    private List<String> maps;
}
