package net.luckyverse.towerrush.events.team;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.events.ArenaEvent;

public abstract class ArenaTeamEvent extends ArenaEvent {

    protected GameTeam team;

    public ArenaTeamEvent(GameArena arena, GameTeam team) {
        super(arena);
        this.team = team;
    }

    public GameTeam getTeam() {
        return team;
    }
}