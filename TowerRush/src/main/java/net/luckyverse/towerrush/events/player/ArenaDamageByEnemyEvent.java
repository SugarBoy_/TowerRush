package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class ArenaDamageByEnemyEvent extends ArenaPlayerEvent implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private final Profile enemy;
    private EntityDamageByEntityEvent basic;

    public ArenaDamageByEnemyEvent(Profile profile, Profile enemy, EntityDamageByEntityEvent basic) {
        super(profile.getArena(), profile);
        this.enemy = enemy;
        this.basic = basic;
        profile.setLastDamager(enemy);
    }

    public Profile getEnemy() {
        return enemy;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public boolean isCancelled() {
        return basic.isCancelled();
    }

    public void setCancelled(boolean b) {
        basic.setCancelled(b);
    }

    public EntityDamageByEntityEvent getBasic() {
        return basic;
    }
}
