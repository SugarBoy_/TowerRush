package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent;

public class ArenaDeathEvent extends ArenaPlayerEvent {

    private static final HandlerList handlers = new HandlerList();
    private final boolean isKilled;
    private final EntityDamageEvent.DamageCause cause;

    public ArenaDeathEvent(Profile profile, EntityDamageEvent.DamageCause cause, boolean isKilled) {
        super(profile.getArena(), profile);
        this.isKilled = isKilled;
        this.cause = cause;

        if (isKilled) {
            Bukkit.getPluginManager().callEvent(new ArenaKillEvent(profile.getLastDamager(), profile));
        }
    }

    public EntityDamageEvent.DamageCause getCause() {
        return cause;
    }

    public boolean isKilled() {
        return isKilled;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
