package net.luckyverse.towerrush.events.team;

import net.luckyverse.towerrush.profile.Profile;
import net.luckyverse.towerrush.shops.upgrade.UpgradeType;
import net.luckyverse.towerrush.shops.upgrade.UpgradeableTier;
import org.bukkit.event.HandlerList;

public class ArenaTeamBuyUpgradeEvent extends ArenaTeamEvent {

    private static final HandlerList handlers = new HandlerList();
    private UpgradeType type;
    private final UpgradeableTier tier;
    private final Profile profile;
    private final int level;

    public ArenaTeamBuyUpgradeEvent(Profile profile, UpgradeType type, UpgradeableTier tier, int level) {
        super(profile.getArena(), profile.getTeam());
        this.profile = profile;
        this.type = type;
        this.tier = tier;
        this.level = level;
    }
    public UpgradeType getType() {
        return type;
    }

    public int getLevel() {
        return level;
    }

    public Profile getProfile() {
        return profile;
    }

    public UpgradeableTier getTier() {
        return tier;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
