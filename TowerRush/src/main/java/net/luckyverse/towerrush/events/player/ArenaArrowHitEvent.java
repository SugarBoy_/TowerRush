package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.entity.Entity;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class ArenaArrowHitEvent extends ArenaDamageByEnemyEvent {

    private static final HandlerList handlers = new HandlerList();

    private final Entity projectile;
    private EntityDamageByEntityEvent basic;

    public ArenaArrowHitEvent(Profile profile, Profile enemy, Entity projectile, EntityDamageByEntityEvent basic) {
        super(profile, enemy, basic);
        this.projectile = projectile;
        this.basic = basic;
        this.profile.setLastDamager(enemy);
    }

    public Entity getProjectile() {
        return projectile;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return basic.isCancelled();
    }

    @Override
    public void setCancelled(boolean b) {
        basic.setCancelled(b);
    }
}
