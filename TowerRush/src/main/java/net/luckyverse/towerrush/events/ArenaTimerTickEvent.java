package net.luckyverse.towerrush.events;

import net.luckyverse.towerrush.arena.GameArena;
import org.bukkit.event.HandlerList;

public class ArenaTimerTickEvent extends ArenaEvent {

    private static final HandlerList handlers = new HandlerList();
    private int time;

    public ArenaTimerTickEvent(GameArena arena, int time) {
        super(arena);
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
