package net.luckyverse.towerrush.events.beacon;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.beacons.GameBeacon;
import net.luckyverse.towerrush.events.ArenaEvent;

public abstract class ArenaBeaconEvent extends ArenaEvent {

    protected GameBeacon beacon;

    public ArenaBeaconEvent(GameArena arena, GameBeacon beacon) {
        super(arena);
        this.beacon = beacon;
    }

    public GameBeacon getBeacon() {
        return beacon;
    }
}
