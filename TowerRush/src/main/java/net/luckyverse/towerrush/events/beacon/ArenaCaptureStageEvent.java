package net.luckyverse.towerrush.events.beacon;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.arena.beacons.GameBeacon;
import org.bukkit.block.Block;
import org.bukkit.event.HandlerList;

public class ArenaCaptureStageEvent extends ArenaBeaconEvent {

    private static final HandlerList handlers = new HandlerList();
    private final GameTeam team;
    private final Block block;

    public ArenaCaptureStageEvent(GameArena arena, GameBeacon beacon, GameTeam team, Block block) {
        super(arena, beacon);
        this.team = team;
        this.block = block;
    }

    public GameTeam getTeam() {
        return team;
    }

    public Block getBlock() {
        return block;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}