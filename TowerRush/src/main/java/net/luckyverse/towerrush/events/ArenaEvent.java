package net.luckyverse.towerrush.events;

import net.luckyverse.towerrush.arena.GameArena;
import org.bukkit.event.Event;

public abstract class ArenaEvent extends Event {


    protected GameArena arena;

    public ArenaEvent(GameArena arena) {
        this.arena = arena;
    }

    public GameArena getArena() {
        return arena;
    }
}
