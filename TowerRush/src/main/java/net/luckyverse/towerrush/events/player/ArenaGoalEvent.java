package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.event.HandlerList;

public class ArenaGoalEvent extends ArenaPlayerEvent {

    private static final HandlerList handlers = new HandlerList();
    private final GameTeam attackedTeam;

    public ArenaGoalEvent(GameArena arena, Profile profile, GameTeam attackedTeam) {
        super(arena, profile);
        this.attackedTeam = attackedTeam;
    }

    public GameTeam getAttackedTeam() {
        return attackedTeam;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
