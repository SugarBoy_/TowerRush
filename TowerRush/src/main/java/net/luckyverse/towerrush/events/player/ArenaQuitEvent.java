package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.event.HandlerList;

public class ArenaQuitEvent extends ArenaPlayerEvent {

    private static final HandlerList handlers = new HandlerList();

    public ArenaQuitEvent(GameArena arena, Profile profile) {
        super(arena, profile);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
