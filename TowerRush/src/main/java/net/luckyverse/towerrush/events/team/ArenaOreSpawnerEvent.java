package net.luckyverse.towerrush.events.team;

import net.luckyverse.towerrush.arena.GameOreSpawner;
import net.luckyverse.towerrush.arena.GameTeam;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.HandlerList;

public class ArenaOreSpawnerEvent extends ArenaTeamEvent {

    private static final HandlerList handlers = new HandlerList();
    private final GameOreSpawner spawner;
    private final Block block;

    public ArenaOreSpawnerEvent(GameTeam team, GameOreSpawner spawner, Block block) {
        super(team.getArena(), team);
        this.spawner = spawner;
        this.block = block;
    }

    public GameOreSpawner getSpawner() {
        return spawner;
    }

    public Block getBlock() {
        return block;
    }

    public void setType(Material material) {
        block.setType(material);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
