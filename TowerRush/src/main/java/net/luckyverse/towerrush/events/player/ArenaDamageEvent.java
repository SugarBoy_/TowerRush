package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent;

public class ArenaDamageEvent extends ArenaPlayerEvent implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private EntityDamageEvent basic;

    public ArenaDamageEvent(Profile profile, EntityDamageEvent basic) {
        super(profile.getArena(), profile);
        this.basic = basic;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return basic.isCancelled();
    }

    @Override
    public void setCancelled(boolean b) {
        basic.setCancelled(b);
    }
}
