package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.event.HandlerList;

public class ArenaKillEvent extends ArenaPlayerEvent {

    private static final HandlerList handlers = new HandlerList();
    private final Profile dead;

    public ArenaKillEvent(Profile killer, Profile dead) {
        super(killer.getArena(), killer);
        this.dead = dead;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public Profile getDead() {
        return dead;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
