package net.luckyverse.towerrush.events.team;

import net.luckyverse.towerrush.arena.GameTeam;
import org.bukkit.event.HandlerList;

public class ArenaTeamLoseEvent extends ArenaTeamEvent {

    private static final HandlerList handlers = new HandlerList();

    public ArenaTeamLoseEvent(GameTeam team) {
        super(team.getArena(), team);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
