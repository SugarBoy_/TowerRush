package net.luckyverse.towerrush.events.beacon;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.beacons.BeaconAction;
import net.luckyverse.towerrush.arena.beacons.GameBeacon;
import org.bukkit.event.HandlerList;

public class ArenaActionChangeEvent extends ArenaBeaconEvent {

    private static final HandlerList handlers = new HandlerList();
    private final BeaconAction old;
    private final BeaconAction action;

    public ArenaActionChangeEvent(GameArena arena, GameBeacon beacon, BeaconAction old, BeaconAction action) {
        super(arena, beacon);
        this.old = old;
        this.action = action;
    }

    public BeaconAction getOldAction() {
        return old;
    }

    public BeaconAction getAction() {
        return action;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}