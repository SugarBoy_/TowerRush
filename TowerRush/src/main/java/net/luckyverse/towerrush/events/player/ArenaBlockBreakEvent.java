package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockBreakEvent;

public class ArenaBlockBreakEvent extends ArenaPlayerEvent implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private BlockBreakEvent basic;

    public ArenaBlockBreakEvent(Profile profile, BlockBreakEvent basic) {
        super(profile.getArena(), profile);
        this.basic = basic;
        basic.setDropItems(false);
        basic.setExpToDrop(0);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public void setCancelled(boolean b) {
        basic.setCancelled(b);
    }

    public Block getBlock() {
        return basic.getBlock();
    }

    public boolean isCancelled() {
        return basic.isCancelled();
    }
}
