package net.luckyverse.towerrush.events.team;

import net.luckyverse.towerrush.arena.GameTeam;
import org.bukkit.event.HandlerList;

public class ArenaTeamWinEvent extends ArenaTeamEvent {

    private static final HandlerList handlers = new HandlerList();

    public ArenaTeamWinEvent(GameTeam team) {
        super(team.getArena(), team);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
