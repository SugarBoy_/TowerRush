package net.luckyverse.towerrush.events.beacon;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.arena.beacons.GameBeacon;
import org.bukkit.event.HandlerList;

public class ArenaCaptureEvent extends ArenaBeaconEvent {

    private static final HandlerList handlers = new HandlerList();
    private final GameTeam team;

    public ArenaCaptureEvent(GameArena arena, GameBeacon beacon, GameTeam team) {
        super(arena, beacon);
        this.team = team;
    }

    public GameTeam getTeam() {
        return team;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}