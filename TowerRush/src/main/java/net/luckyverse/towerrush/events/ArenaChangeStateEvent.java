package net.luckyverse.towerrush.events;

import net.luckyverse.towerrush.TowerRush;
import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameState;
import org.bukkit.event.HandlerList;

public class ArenaChangeStateEvent extends ArenaEvent {

    private static final HandlerList handlers = new HandlerList();
    private final GameState old;
    private final GameState state;

    public ArenaChangeStateEvent(GameArena arena, GameState old, GameState state) {
        super(arena);
        this.old = old;
        this.state = state;
        TowerRush.logger().info(GameArena.class, String.format("Арена %s теперь в режиме %s.",
                arena.ARENA_NAME, state.name()));
    }

    public GameState getOldState() {
        return old;
    }

    public GameState getState() {
        return state;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
