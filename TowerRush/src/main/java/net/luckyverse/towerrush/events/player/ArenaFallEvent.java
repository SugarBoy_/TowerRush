package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.event.HandlerList;

public class ArenaFallEvent extends ArenaPlayerEvent {

    private static final HandlerList handlers = new HandlerList();

    public ArenaFallEvent(Profile profile) {
        super(profile.getArena(), profile);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
