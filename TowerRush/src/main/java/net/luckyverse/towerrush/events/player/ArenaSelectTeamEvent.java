package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.event.HandlerList;

public class ArenaSelectTeamEvent extends ArenaPlayerEvent {

    private static final HandlerList handlers = new HandlerList();
    private final GameTeam team;

    public ArenaSelectTeamEvent(GameArena arena, Profile profile, GameTeam team) {
        super(arena, profile);
        this.team = team;
    }

    public GameTeam getTeam() {
        return team;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
