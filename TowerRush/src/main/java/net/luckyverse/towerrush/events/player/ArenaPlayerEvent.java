package net.luckyverse.towerrush.events.player;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.events.ArenaEvent;
import net.luckyverse.towerrush.profile.Profile;

public abstract class ArenaPlayerEvent extends ArenaEvent {

    protected Profile profile;

    public ArenaPlayerEvent(GameArena arena, Profile profile) {
        super(arena);
        this.profile = profile;
    }

    public Profile getProfile() {
        return profile;
    }
}
