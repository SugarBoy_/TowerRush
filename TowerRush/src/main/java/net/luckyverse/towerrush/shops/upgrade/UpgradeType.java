package net.luckyverse.towerrush.shops.upgrade;

public enum UpgradeType {

    SWORD, // Деревянный меч, каменный меч, железный мея
    BOW, // Сила I, Сила II, Сила III, Сила II bи заговор огня
    ARMOR, // Кожанная, кальчуга, золотая
    ARROWS, // 2 Стрелы, 3  стрелы, 4 стрелы
    PICKAXE, // Открытие вкладки инструменты, каменная кирка и топор, железные
    POTIONS, // Тут сам можешь придумать
    ORE_SPEED,
    ORE_QUALITY,
    ORE_COUNT

}
