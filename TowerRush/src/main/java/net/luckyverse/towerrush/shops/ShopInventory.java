package net.luckyverse.towerrush.shops;

import net.luckyverse.sugarboy.inventories.MyInventory;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public abstract class ShopInventory extends MyInventory {

    public ShopInventory(Player player, int rows, String name) {
        super(player, rows, name);
    }

    public void setItem(int slot, ItemStack item, int emerald, int gold, int iron) {
        ItemStack icon = item.clone();
        Runnable runnable = () -> {
            if (!item.getType().equals(Material.BARRIER)) {
                PlayerInventory inv = player.getInventory();
                if (inv.contains(Material.EMERALD, emerald) && inv.contains(Material.GOLD_INGOT, gold) &&
                        inv.contains(Material.IRON_INGOT, iron)) {

                    inv.removeItem(new ItemStack(Material.EMERALD, emerald),
                            new ItemStack(Material.GOLD_INGOT, gold),
                            new ItemStack(Material.IRON_INGOT, iron));

                    inv.addItem(icon);
                } else {
                    player.sendMessage("§cНедостаточно ресурсов для покупки!");
                }
            } else {
                player.sendMessage("§cДанный предмет пока не доступен!");
            }
        };

        ItemMeta meta = item.clone().getItemMeta();
        List<String> lore = meta.getLore();
        if (lore == null)
            lore = new ArrayList<>();
        lore.add("");

        String coast = "§7Стоимость: ";
        coast += (iron == 0 ? "" : String.format("§f■×%d ", iron)) +
                (gold == 0 ? "" : String.format("§6■×%d ", gold) +
                (emerald == 0 ? "" : String.format("§2■×%d ", emerald)));
        lore.add(coast);
        meta.setLore(lore);
        item.setItemMeta(meta);

        setItem(slot, item, runnable);
    }

    public void eventClose() {}
}
