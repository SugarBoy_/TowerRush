package net.luckyverse.towerrush.shops.upgrade;

import net.luckyverse.towerrush.TowerRush;
import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.arena.listeners.GuiListener;
import net.luckyverse.towerrush.events.team.ArenaTeamBuyUpgradeEvent;
import net.luckyverse.towerrush.profile.Profile;
import net.luckyverse.towerrush.utils.configurations.UpgradeMenuSettings;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class UpgradeSystemCore {

    private HashMap<GameTeam, Map<UpgradeType, Integer>> map = new HashMap<>();

    private GameArena arena;
    private UpgradeMenuSettings settings;
    private Set<UpgradableItem> upgrades = new HashSet<>();

    public UpgradeSystemCore(GameArena arena) {
        this.arena = arena;
        this.settings = TowerRush.plugin().getSettings().upgradeMenuSettings;

        for (GameTeam team : arena.getTeams().map().values()) {
            HashMap<UpgradeType, Integer> ups = new HashMap<>();
            for (UpgradeType type : UpgradeType.values())
                ups.put(type, 0);
            map.put(team, ups);
        }

        upgrades.addAll(settings.getUpgrades());
        upgrades.stream().forEach(upgrade -> upgrade.init(this));
    }

    public void open(Profile profile) {
        Inventory inv = Bukkit.createInventory(null, 9 * settings.getRows(), "§8Доступные улучшения:");

        boolean notClear = false;
        for (UpgradableItem upgrade : upgrades) {
            UpgradeableTier tier = upgrade.getTier(profile.getTeam());
            if (tier == null)
                continue;
            inv.setItem(upgrade.getSlot(), castTierInItem(tier));
            notClear = true;
        }

        if (notClear) {
            GuiListener.addUpgradesMenu(inv, profile);
            profile.getPlayer().openInventory(inv);
        } else {
            profile.getPlayer().closeInventory();
            profile.getPlayer().sendMessage("§cВсе улучшения были куплены.");
        }
    }

    public int getLevel(GameTeam team, UpgradeType type) {
        return map.get(team).get(type);
    }

    public void buy(Profile profile, UpgradeType type, UpgradeableTier tier) {
        GameTeam team = profile.getTeam();
        Map<UpgradeType, Integer> teamMap = map.get(team);
        int level = teamMap.get(type) + 1;
        Bukkit.getPluginManager().callEvent(new ArenaTeamBuyUpgradeEvent(profile, type, tier, level));
        teamMap.put(type, level);
    }

    public UpgradableItem getUpgradeBySlot(int slot) {
        return upgrades.stream().filter(i -> i.getSlot() == slot).findFirst().orElse(null);
    }

    public UpgradableItem getUpgradeByType(UpgradeType type) {
        return upgrades.stream().filter(i -> i.getType().equals(type)).findFirst().get();
    }

    private ItemStack castTierInItem(UpgradeableTier tier) {
        ItemStack item = tier.getItem().clone();
        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>();
        String coast = "§7Стоимость: ";
        coast += (tier.getIron() == 0 ? "" : String.format("§f■×%d ", tier.getIron())) +
                (tier.getGold() == 0 ? "" : String.format("§6■×%d ", tier.getGold())) +
                (tier.getEmerald() == 0 ? "" : String.format("§2■×%d ", tier.getEmerald()));
        lore.add(coast);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES);
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }

}
