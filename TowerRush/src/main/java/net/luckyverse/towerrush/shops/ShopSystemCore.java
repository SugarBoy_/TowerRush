package net.luckyverse.towerrush.shops;

import net.luckyverse.sugarboy.inventories.MyInventory;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.profile.Profile;
import net.luckyverse.towerrush.shops.upgrade.UpgradeSystemCore;
import net.luckyverse.towerrush.shops.upgrade.UpgradeType;
import net.luckyverse.towerrush.utils.configurations.BukkitColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.util.Arrays;
import java.util.List;

import static org.bukkit.Material.*;

public class ShopSystemCore {

    private UpgradeSystemCore upgrades;

    public ShopSystemCore(UpgradeSystemCore upgrades) {
        this.upgrades = upgrades;
    }

    public void open(Profile profile) {
        new MyInventory(profile.getPlayer(), 3, "§8Выберите категорию предметов:") {
            public void eventOpen() {
                ItemStack blocks = item(new ItemStack(Material.BRICK), "§eБлоки",null);
                ItemStack equp = item(new ItemStack(Material.IRON_SWORD), "§eОружие и броня", null);
                ItemStack pickaxe = item(new ItemStack(Material.STONE_PICKAXE), "§eИнвентарь", null);
                ItemStack potions = item(new ItemStack(Material.POTION), "§eЗелья", null);
                ItemStack others = item(new ItemStack(Material.FIREBALL), "§eРазное", null);

                setItem(11, blocks, () -> openBlock(profile));
                setItem(12, equp, () -> openEquip(profile));
                setItem(13, pickaxe, () -> openPickaxe(profile));
                setItem(14, potions, () -> openPotions(profile));
                setItem(15, others, () -> openOthers(profile));
            }
            public void eventClose() {}
        };
    }

    private void openBlock(Profile profile) {
        new ShopInventory(profile.getPlayer(), 3, "§8Выберите тип блоков:") {
            public void eventOpen() {
                GameTeam team = profile.getTeam();
                                
                ItemStack glass = item(new ItemStack(Material.STAINED_GLASS, 16, (short) team.getByteCode()), "§eСтекло",null);
                ItemStack wool = item(new ItemStack(Material.WOOL, 8, (short) team.getByteCode()), "§eШерсть",null);
                ItemStack gray = item(new ItemStack(Material.STAINED_CLAY, 8, (short) team.getByteCode()), "§eГлина",null);


                setItem(12, glass, 0, 0, 4);
                setItem(13, wool, 0, 0, 16);
                setItem(14, gray, 0, 0, 32);
            }
        };
    }

    private void openEquip(Profile profile) {
        new ShopInventory(profile.getPlayer(), 3, "§8Выберите тип экипировки:") {
            public void eventOpen() {
                GameTeam team = profile.getTeam();

                /*-----------------------------------------------------------*/
                Material sword = Material.WOOD_AXE;

                switch (upgrades.getLevel(team, UpgradeType.SWORD)) {
                    case 1:
                        sword = Material.WOOD_SWORD;
                        break;
                    case 2:
                        sword = Material.STONE_SWORD;
                        break;
                    case 3:
                        sword = Material.IRON_SWORD;
                        break;
                }
                ItemStack itemSword = item(setUnbreakable(new ItemStack(sword)), "§eМеч", null);

                /*-----------------------------------------------------------*/

                List<String> lore = Arrays.asList("§7Данный предмет открывается", "§7после покупки улучшения.");

                int levelBow = upgrades.getLevel(team, UpgradeType.BOW);
                ItemStack itemBow = item(new ItemStack(levelBow != 0 ? BOW : BARRIER), "§eЛук", lore);
                if (levelBow > 1) {
                    ItemMeta meta = itemBow.getItemMeta();
                    meta.addEnchant(Enchantment.ARROW_DAMAGE, levelBow-1, true);
                    meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    meta.setUnbreakable(true);
                    itemBow.setItemMeta(meta);
                }

                /*-----------------------------------------------------------*/
                Material helmet = BARRIER;
                Material leggings = BARRIER;
                Material boots = BARRIER;


                switch (upgrades.getLevel(team, UpgradeType.ARMOR)) {
                    case 1:
                        helmet = Material.LEATHER_HELMET;
                        leggings = Material.LEATHER_LEGGINGS;
                        boots = Material.LEATHER_BOOTS;
                        break;
                    case 2:
                        helmet = Material.CHAINMAIL_HELMET;
                        leggings = Material.CHAINMAIL_LEGGINGS;
                        boots = Material.CHAINMAIL_BOOTS;
                        break;
                    case 3:
                        helmet = Material.IRON_HELMET;
                        leggings = Material.IRON_LEGGINGS;
                        boots = Material.IRON_BOOTS;
                        break;
                }
                ItemStack itemHelmet = item(setUnbreakable(new ItemStack(helmet)), "§eШлем", lore);
                ItemStack itemLeggings = item(setUnbreakable(new ItemStack(leggings)), "§eШтаны", lore);
                ItemStack itemBoots = item(setUnbreakable(new ItemStack(boots)), "§eБотинки", lore);

                ItemStack itemArrows = item(new ItemStack(ARROW, (int) Math.pow(2, upgrades.getLevel(team, UpgradeType.ARROWS))), "§eСтрелы", null);

                setItem(10, itemSword, 0, 2, 4);
                setItem(11, itemBow, 0, 12, 24);
                setItem(12, itemHelmet, 0, 1, 5);
                setItem(13, itemLeggings, 0, 1, 7);
                setItem(14, itemBoots, 0, 1, 4);
                setItem(15, itemArrows, 0, 1, 0);
            }
        };
    }

    private void openPickaxe(Profile profile) {
        new ShopInventory(profile.getPlayer(), 3, "§8Выберите инструменты:") {
            public void eventOpen() {
                GameTeam team = profile.getTeam();

                /*-----------------------------------------------------------*/
                Material pickaxe = Material.IRON_PICKAXE;

                int level = upgrades.getLevel(team, UpgradeType.PICKAXE);
                ItemStack itemPickaxe = item(setUnbreakable(new ItemStack(pickaxe)), "§eКирка", null);
                if (level != 0) {
                    ItemMeta itemMeta = itemPickaxe.getItemMeta();
                    itemMeta.addEnchant(Enchantment.DIG_SPEED, level, true);
                    itemPickaxe.setItemMeta(itemMeta);
                }

                /*-----------------------------------------------------------*/

                ItemStack itemShears = item(setUnbreakable(new ItemStack(SHEARS)), "§eНожницы", null);
                ItemStack itemShield = item(setUnbreakable(new ItemStack(SHIELD)), "§eЩит", null);

                setItem(12, itemShears, 0, 4, 12);
                setItem(13, itemPickaxe, 0, 4, 0);
                setItem(14, itemShield, 0, 2, 6);
            }
        };
    }

    private void openPotions(Profile profile) {
        new ShopInventory(profile.getPlayer(), 3, "§8Выберите тип зелий:") {
            public void eventOpen() {
                GameTeam team = profile.getTeam();

                int level = upgrades.getLevel(team, UpgradeType.POTIONS);

                ItemStack itemMilk = item(new ItemStack(MILK_BUCKET), "§eМолочко", null);

                if (level >= 1) {
                    ItemStack potion1 = item(new ItemStack(POTION), "§eЗелье скорости", null);
                    PotionMeta meta1 = (PotionMeta) potion1.getItemMeta();
                    meta1.setBasePotionData(new PotionData(PotionType.SPEED, false, true));
                    meta1.setColor(BukkitColor.AQUA.getColor());
                    potion1.setItemMeta(meta1);
                    setItem(10, potion1, 0, 3, 12);

                    ItemStack potion2 = item(new ItemStack(POTION), "§eЗелье прыгучести", null);
                    PotionMeta meta2 = (PotionMeta) potion2.getItemMeta();
                    meta2.setBasePotionData(new PotionData(PotionType.JUMP, false, true));
                    meta2.setColor(BukkitColor.GREEN.getColor());
                    potion2.setItemMeta(meta2);
                    setItem(11, potion2, 0, 2, 8);
                }
                if (level >= 2) {
                    ItemStack potion1 = item(new ItemStack(POTION), "§eЗелье регенерации", null);
                    PotionMeta meta1 = (PotionMeta) potion1.getItemMeta();
                    meta1.setBasePotionData(new PotionData(PotionType.REGEN, false, true));
                    meta1.setColor(BukkitColor.RED.getColor());
                    potion1.setItemMeta(meta1);
                    setItem(12, potion1, 1, 6, 12);

                    ItemStack potion2 = item(new ItemStack(POTION), "§eЗелье исцеления", null);
                    PotionMeta meta2 = (PotionMeta) potion2.getItemMeta();
                    meta2.setBasePotionData(new PotionData(PotionType.INSTANT_HEAL, false, true));
                    meta2.setColor(BukkitColor.ORANGE.getColor());
                    potion2.setItemMeta(meta2);
                    setItem(13, potion2, 0, 3, 8);
                }
                if (level >= 3) {
                    ItemStack potion1 = item(new ItemStack(POTION), "§eЗелье силы", null);
                    PotionMeta meta1 = (PotionMeta) potion1.getItemMeta();
                    meta1.setBasePotionData(new PotionData(PotionType.STRENGTH, false, true));
                    meta1.setColor(BukkitColor.DARK_BLUE.getColor());
                    potion1.setItemMeta(meta1);
                    setItem(14, potion1, 6, 12, 24);

                    ItemStack potion2 = item(new ItemStack(SPLASH_POTION), "§eЗелье отравления", null);
                    PotionMeta meta2 = (PotionMeta) potion2.getItemMeta();
                    meta2.setBasePotionData(new PotionData(PotionType.POISON, false, false));
                    meta2.setColor(BukkitColor.DARK_GREEN.getColor());
                    potion2.setItemMeta(meta2);
                    setItem(15, potion2, 4, 2, 24);

                    ItemStack potion3 = item(new ItemStack(SPLASH_POTION), "§eЗелье усталости", null);
                    PotionMeta meta3 = (PotionMeta) potion3.getItemMeta();
                    meta3.setBasePotionData(new PotionData(PotionType.WEAKNESS, false, false));
                    meta3.setColor(BukkitColor.GRAY.getColor());
                    potion3.setItemMeta(meta3);
                    setItem(16, potion3, 0, 2, 48);
                }

                setItem(level == 0 ? 10 : 22, itemMilk, 0, 1, 0);
            }
        };
    }

    private void openOthers(Profile profile) {
        new ShopInventory(profile.getPlayer(), 3, "§8Выберите тип предмета:") {
            public void eventOpen() {
                ItemStack apple = item(new ItemStack(Material.GOLDEN_APPLE), "§eЗолотое яблоко",null);
                ItemStack tnt = item(new ItemStack(Material.TNT), "§eДинамит",null);
                ItemStack pearl = item(new ItemStack(Material.ENDER_PEARL), "§eОко эндера",null);

                setItem(12, apple, 0, 2, 8);
                setItem(13, tnt, 0, 4, 6);
                setItem(14, pearl, 16, 32, 64);
            }
        };
    }

    private ItemStack setUnbreakable(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta.setUnbreakable(true);
        item.setItemMeta(meta);
        return item;
    }
}
