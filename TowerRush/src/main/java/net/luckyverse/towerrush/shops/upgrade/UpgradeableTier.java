package net.luckyverse.towerrush.shops.upgrade;

import com.google.gson.annotations.Expose;
import org.bukkit.inventory.ItemStack;

public class UpgradeableTier {

    @Expose
    private int iron = 0;

    @Expose
    private int gold = 0;

    @Expose
    private int emerald = 0;

    @Expose
    private ItemStack item;

    public int getIron() {
        return iron;
    }

    public int getGold() {
        return gold;
    }

    public int getEmerald() {
        return emerald;
    }

    public ItemStack getItem() {
        return item;
    }
}
