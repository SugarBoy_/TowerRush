package net.luckyverse.towerrush.shops.upgrade;

import com.google.gson.annotations.Expose;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.profile.Profile;

import java.util.List;

public class UpgradableItem {

    @Expose
    private String name;

    @Expose
    private int slot;

    @Expose
    private List<UpgradeableTier> tiers;
    private UpgradeSystemCore menu;

    public void init(UpgradeSystemCore menu) {
        this.menu = menu;
    }

    public UpgradeType getType() {
        return UpgradeType.valueOf(name.toUpperCase());
    }

    public UpgradeableTier getTier(GameTeam team) {
        int level = menu.getLevel(team, getType());
        return tiers.size() != level ? tiers.get(menu.getLevel(team, getType())) : null;
    }

    public void buy(Profile profile) {
    }

    public int getSlot() {
        return slot;
    }
}
