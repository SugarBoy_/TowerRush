package net.luckyverse.towerrush.profile;

import net.luckyverse.towerrush.TowerRush;
import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.events.player.ArenaRespawnEvent;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import ru.yooxa.connecter.Core;

public class Profile {

    private Player player;
    private GameArena arena;
    private GameTeam team = null;
    private boolean alive = true;
    private Profile lastDamager;

    public Profile(Player player, GameArena arena) {
        this.player = player;
        this.arena = arena;
        player.setDisplayName("§7" + player.getName());
    }

    public void setTeam(GameTeam team) {
        this.team = team;
    }

    public void death() {
        alive = false;
        player.setGameMode(GameMode.SPECTATOR);
        player.setHealth(player.getMaxHealth());
        player.closeInventory();

        int respawnTime = arena.getArenaConfig().timeToRespawn;

        new Thread(()-> {
            int q = respawnTime;
            while (q != 0) {
                player.sendTitle("§c" + q, "", 0, 20, 0);
                q--;
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {}
            }
        }).start();

        arena.addTask(Bukkit.getScheduler().runTaskLater(TowerRush.plugin(), ()-> {
            alive = true;
            if (player.isOnline() && team != null && !team.isLose()) {
                spawn();
            }
        }, respawnTime * 20L));
    }

    public void spawn() {
        player.getInventory().clear();
        player.teleport(team.getSpawn());
        player.setGameMode(GameMode.SURVIVAL);
        player.setExp(0);
        player.setLevel(0);
        Bukkit.getPluginManager().callEvent(new ArenaRespawnEvent(this));
    }

    public void setLastDamager(Profile lastDamager) {
        this.lastDamager = lastDamager;
    }

    public void toLobby() {
        if (player.isOnline())
            Core.redirect(player, "KoT-Lobby-1");
    }

    public Profile getLastDamager() {
        return lastDamager;
    }

    public GameTeam getTeam() {
        return team;
    }

    public Player getPlayer() {
        return player;
    }

    public GameArena getArena() {
        return arena;
    }

    public boolean isAlive() {
        return alive;
    }
}
