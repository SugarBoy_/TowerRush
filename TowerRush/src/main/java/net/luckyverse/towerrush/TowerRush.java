package net.luckyverse.towerrush;

import com.google.gson.reflect.TypeToken;
import net.luckyverse.sugarboy.Logger;
import net.luckyverse.sugarboy.configurations.converters.ItemStackConverter;
import net.luckyverse.sugarboy.configurations.converters.LocationConverter;
import net.luckyverse.sugarboy.files.MyFileUtils;
import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.listeners.*;
import net.luckyverse.towerrush.commands.CommandTowerRush;
import net.luckyverse.towerrush.commands.CommandWorld;
import net.luckyverse.towerrush.stores.ArenaStore;
import net.luckyverse.towerrush.utils.configurations.ArenaSettings;
import net.luckyverse.towerrush.utils.configurations.ConfigurationObject;
import net.luckyverse.towerrush.utils.configurations.GlobalSettings;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldCreator;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collections;
import java.util.List;

public class TowerRush extends JavaPlugin {

    private static TowerRush plugin;
    private static Logger logger;

    private GlobalSettings config;
    private ArenaStore arenaStore;
    private String serverName;
    private CoreHandler proxy;

    public static TowerRush plugin() {
        return plugin;
    }

    public static Logger logger() {
        return logger;
    }

    public void onEnable() {
        plugin = this;
        logger = new Logger(this);

        /*-----------------------------------------*/
        //
        //    Load configs
        //
        /*-----------------------------------------*/

        loadWorldHowBidlo();
        reloadConfigs();

        /*-----------------------------------------*/
        //
        //    Initialization arenas
        //
        /*-----------------------------------------*/

        arenaStore = new ArenaStore();

        List<ArenaSettings> list = config.arenaSettings;
        Collections.shuffle(list);
        for (ArenaSettings settings : list) {
            arenaStore.initArena(settings, false);
        }

        //MyFileUtils.printJsonFormat(getArenaConfig);

        /*-----------------------------------------*/
        //
        //    Load commands
        //
        /*-----------------------------------------*/

        this.getCommand("world").setExecutor(new CommandWorld());
        this.getCommand("towerrush").setExecutor(new CommandTowerRush(arenaStore));

        /*-----------------------------------------*/
        //
        //    Starting listeners
        //
        /*-----------------------------------------*/

        Bukkit.getPluginManager().registerEvents(new GlobalListener(arenaStore, config), this);
        Bukkit.getPluginManager().registerEvents(new GuiListener(arenaStore), this);
        Bukkit.getPluginManager().registerEvents(new GameListener(), this);
        Bukkit.getPluginManager().registerEvents(new BeaconListener(), this);
        Bukkit.getPluginManager().registerEvents(new NotificationListener(), this);

        /*-----------------------------------------*/
        //
        //    Connection to core
        //
        /*-----------------------------------------*/

        String[] data = Bukkit.getWorldContainer().getAbsolutePath().split("/");
        serverName = data[data.length-2];
        logger.info(TowerRush.class, "Имя сервера: " + serverName);
        proxy = new CoreHandler(arenaStore);

    }

    public void onDisable() {
        arenaStore.map().values().forEach(arena -> arena.unload());
    }

    private void loadWorldHowBidlo() {
        reloadConfigs().arenaSettings.stream().map(i -> i.world).forEach(world -> {
            MyFileUtils.copySchemWorld(this,"worlds", world);
            TowerRush.plugin().getServer().createWorld(new WorldCreator(world));
        });
    }

    public GlobalSettings reloadConfigs() {
        return this.config = ((ConfigurationObject<GlobalSettings>) new ConfigurationObject()
                .addAdapter(ItemStack.class, new ItemStackConverter())
                .addAdapter(Location.class, new LocationConverter())
                .load("configs", "settings.json", new TypeToken<GlobalSettings>(){})
        ).read();
    }

    public void reloadConfig() {
        reloadConfigs();
    }

    public GlobalSettings getSettings() {
        return config;
    }

    public String getServerName() {
        return serverName;
    }
}
