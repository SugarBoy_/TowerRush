package net.luckyverse.towerrush.utils.configurations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.List;

public class GlobalSettings {

    @Expose
    @SerializedName("lobby")
    public Location locLobby = new Location(Bukkit.getWorld("world"), 0, 100, 0);

    @Expose
    @SerializedName("arenas")
    public List<ArenaSettings> arenaSettings;

    @Expose
    @SerializedName("upgrades")
    public UpgradeMenuSettings upgradeMenuSettings;

}
