package net.luckyverse.towerrush.utils.configurations;

import net.luckyverse.sugarboy.cosmetics.Board;
import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;

public class GameBoard extends Board {

    private GameArena arena;

    public GameBoard(GameArena arena) {
        super("§c§lKing of the Tower");
        this.arena = arena;

        setLine(16, " ");

        int q = 15;
        for (GameTeam team : arena.getTeams().map().values()) {
            setLine(q, team.getColor() + team.getName() + ": ");
            q--;
        }

        setLine(q--, " ");
        setLine(q--, "§7Время игры: §e");
    }

    public void uppdate() {
        int q = 15;
        for (GameTeam team : arena.getTeams().map().values()) {
            updateSuffix(q, team.isLose() ? "§4✗" : String.format("§4❤§7×§f%d", team.getHealth()));
            q--;
        }

        q--;
        updateSuffix(q--, String.format("%.1f мин", arena.time / 60F));
    }
}
