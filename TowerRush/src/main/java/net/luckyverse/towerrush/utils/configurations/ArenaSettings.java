package net.luckyverse.towerrush.utils.configurations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.arena.beacons.GameBeacon;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class ArenaSettings {

    @Expose
    public String name = "KoT-1";

    @Expose
    @SerializedName("loadable-world")
    public String world = "arena_1";

    @Expose
    public String map = "unnamed";

    @Expose
    @SerializedName("location-lobby")
    public Location locLobby = new Location(Bukkit.getWorld("world"), 0, 100, 0);

    @Expose
    @SerializedName("min-players-for-start")
    public int minPlayers = 6;

    @Expose
    @SerializedName("time-to-start")
    public int timeToStart = 120;

    @Expose
    @SerializedName("respawn-time")
    public int timeToRespawn = 5;

    @Expose
    @SerializedName("getHealth-for-win")
    public int winHealth = 8;

    @Expose
    public List<GameTeam> teams = new ArrayList<>();

    @Expose
    public List<GameBeacon> beacons = new ArrayList<>();
}
