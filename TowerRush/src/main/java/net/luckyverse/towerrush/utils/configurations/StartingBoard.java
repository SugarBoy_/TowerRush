package net.luckyverse.towerrush.utils.configurations;

import net.luckyverse.sugarboy.cosmetics.Board;
import net.luckyverse.towerrush.arena.GameArena;

public class StartingBoard extends Board {

    private GameArena arena;

    public StartingBoard(GameArena arena) {
        super("§c§lKing of the Tower");
        this.arena = arena;

        setLine(5, " ");
        setLine(4, "§7Карта: §e" + arena.getArenaConfig().map);
        setLine(3, " ");
        setLine(2, "§7Игроков: §e");
        setLine(1, "");
        setLine(0, "§7Начало через: §e");
    }

    public void uppdate() {
        updateSuffix(2, arena.getProfiles().size() + "§8/§e" + arena.getLimit());
        updateSuffix(0, Math.abs(arena.time) + " секунд");
    }
}
