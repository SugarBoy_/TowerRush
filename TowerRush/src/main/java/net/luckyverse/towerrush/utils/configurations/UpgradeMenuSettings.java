package net.luckyverse.towerrush.utils.configurations;

import com.google.gson.annotations.Expose;
import net.luckyverse.towerrush.shops.upgrade.UpgradableItem;

import java.util.List;

public class UpgradeMenuSettings {

    @Expose
    private int rows = 5;

    @Expose
    private List<UpgradableItem> upgrades;

    public int getRows() {
        return rows;
    }

    public List<UpgradableItem> getUpgrades() {
        return upgrades;
    }
}
