package net.luckyverse.towerrush.utils.configurations;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import net.luckyverse.sugarboy.configurations.Configuration;
import net.luckyverse.sugarboy.files.MyFileUtils;
import net.luckyverse.towerrush.TowerRush;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.FileReader;
import java.util.List;

public class ConfigurationList<T> implements Configuration {

    // Main configuration directory
    private static final String OWN_DIR = Bukkit.getWorldContainer().getPath() + "/";

    private File configFile;
    private List<T> list;
    private GsonBuilder gsonBuilder = new GsonBuilder();

    /**
     * Добавить кастомный сериализатор к основному
     *
     * @param clazz - Класс, к которому привязать сериализатор
     * @param adapter - Сам сериализатор
     *
     * @return вернет преобразованный объект данного класса
     * */
    public ConfigurationList addAdapter(Class clazz, JsonSerializer adapter) {
        gsonBuilder = gsonBuilder.registerTypeAdapter(clazz, adapter);
        return this;
    }

    @Override
    public ConfigurationList<T> load(String dir, String file, TypeToken typeToken) {
        String source = String.format("%s/%s", dir, file);

        try {
            new File(OWN_DIR).mkdirs();
            configFile = MyFileUtils.autoCreate(source, OWN_DIR + source);
            list = gsonBuilder.create().fromJson(new FileReader(configFile), typeToken.getType());
            TowerRush.logger().info(Configuration.class, String.format("Конфигурация %s успешно загружена", file));
        } catch (Exception e) {
            TowerRush.logger().error(Configuration.class, String.format("Ошибка загрузки конфигурации %s", source));
            e.printStackTrace();
        }

        return this;
    }

    @Override
    public List<T> read() {
        return list;
    }
}
