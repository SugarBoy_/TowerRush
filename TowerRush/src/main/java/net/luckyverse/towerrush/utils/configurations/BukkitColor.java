package net.luckyverse.towerrush.utils.configurations;

import org.bukkit.Color;

import java.util.Arrays;

public enum  BukkitColor {

    WHITE(0, 255, 255, 255),
    ORANGE(1, 255, 157, 0),
    PURPLE(2, 225, 0, 255),
    BLUE(3, 111, 170, 247),
    YELLOW(4, 245, 237, 10),
    GREEN(5, 55, 214, 30),
    PINK(6, 245, 130, 231),
    DARK_GRAY(7, 94, 94, 94),
    GRAY(8, 201, 201, 201),
    AQUA(9, 40, 214, 237),
    DARK_PURPLE(10, 293, 99, 68),
    DARK_BLUE(11, 22, 66, 224),
    BROWN(12, 117, 58, 15),
    DARK_GREEN(13, 27, 127, 33),
    RED(14, 189, 11, 11),
    BLACK(15, 0, 0, 0);


    private final int byteCode;
    private final int red;
    private final int green;
    private final int blue;

    BukkitColor(int byteCode, int red, int green, int blue) {
        this.byteCode = byteCode;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public int getByteCode() {
        return byteCode;
    }

    public Color getColor() {
        return Color.fromRGB(red, green, blue);
    }

    public static BukkitColor getByCode(int id) {
        return id >= 0 && id < 16 ? Arrays.stream(values()).filter(c -> c.byteCode == id).findFirst().get() : null;
    }
}
