package net.luckyverse.towerrush.arena;

public enum GameState {

    WAITING(0),
    STARTING(1),
    PLAYING(2),
    ENDING(3),
    RESTARTING(4);

    private int number;

    GameState(int number) {
        this.number = number;
    }

    public int number() {
        return number;
    }
}
