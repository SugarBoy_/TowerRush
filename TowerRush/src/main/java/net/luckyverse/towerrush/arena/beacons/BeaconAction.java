package net.luckyverse.towerrush.arena.beacons;

public enum BeaconAction {

    SPEED("Ускорение гепарда"),
    DOUBLE_EMERALDS("Изумрудная лихорадка"),
    JUMPING("Кузнечик"),
    LIFESTEAL("Высасывание крови"),
    NULL("Нет");

    private String name;

    BeaconAction(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return name;
    }
}
