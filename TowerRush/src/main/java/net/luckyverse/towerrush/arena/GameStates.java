package net.luckyverse.towerrush.arena;

public enum GameStates {

    WAITING(0),
    STARTING(1),
    PLAYING(2),
    ENDING(3),
    RESTARTING(3);

    private int number;

    GameStates(int number) {
        this.number = number;
    }

    public int number() {
        return number;
    }
}
