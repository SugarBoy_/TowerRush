package net.luckyverse.towerrush.arena.listeners;

import net.luckyverse.towerrush.TowerRush;
import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameOreSpawner;
import net.luckyverse.towerrush.arena.GameState;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.arena.beacons.BeaconAction;
import net.luckyverse.towerrush.arena.beacons.GameBeacon;
import net.luckyverse.towerrush.events.ArenaChangeStateEvent;
import net.luckyverse.towerrush.events.ArenaTimerTickEvent;
import net.luckyverse.towerrush.events.beacon.ArenaActionChangeEvent;
import net.luckyverse.towerrush.events.beacon.ArenaCaptureEvent;
import net.luckyverse.towerrush.events.beacon.ArenaCaptureStageEvent;
import net.luckyverse.towerrush.events.player.*;
import net.luckyverse.towerrush.events.team.ArenaOreSpawnerEvent;
import net.luckyverse.towerrush.events.team.ArenaTeamBuyUpgradeEvent;
import net.luckyverse.towerrush.events.team.ArenaTeamLoseEvent;
import net.luckyverse.towerrush.events.team.ArenaTeamWinEvent;
import net.luckyverse.towerrush.profile.Profile;
import net.luckyverse.towerrush.shops.upgrade.UpgradeType;
import net.luckyverse.towerrush.utils.configurations.BukkitColor;
import net.luckyverse.towerrush.utils.configurations.GameBoard;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;
import java.util.stream.Collectors;

public class GameListener implements Listener {

    private static final Random rnd = new Random();

    private static ItemStack itemPickAxe;
    private static ItemStack itemChestPlate;
    private static ItemStack itemAxe;

    public GameListener() {
        itemPickAxe = new ItemStack(Material.IRON_PICKAXE);
        ItemMeta meta = itemPickAxe.getItemMeta();
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta.setDisplayName("§eКирка");
        itemPickAxe.setItemMeta(meta);

        itemChestPlate = new ItemStack(Material.LEATHER_CHESTPLATE);
        meta = itemChestPlate.getItemMeta();
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta.setDisplayName("§eНагрудник");
        itemChestPlate.setItemMeta(meta);

        itemAxe = new ItemStack(Material.WOOD_AXE);
        meta = itemAxe.getItemMeta();
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta.setDisplayName("§eТопор");
        itemAxe.setItemMeta(meta);
    }

    @EventHandler
    public void onJoin(ArenaJoinEvent e) {
        Profile profile = e.getProfile();
        Player player = profile.getPlayer();
        GameArena arena = e.getArena();

        for (Player all : Bukkit.getOnlinePlayers()) {
            player.hidePlayer(TowerRush.plugin(), all);
            all.hidePlayer(TowerRush.plugin(), player);
        }

        if (arena.isGame()) {

        } else {

            arena.broadcast(String.format("§f[§c§lKing of the Tower§f]: §e%s §7присоединился к игре: [§e%d§7/§e%d§7]",
                    player.getName(), arena.getProfiles().size(), arena.getLimit()));

            player.teleport(arena.getArenaConfig().locLobby);

            player.setFlying(false);
            player.setAllowFlight(false);
            player.setGameMode(GameMode.ADVENTURE);
            player.setHealth(20);
            player.setTotalExperience(0);
            player.setFoodLevel(20);
            player.setExp(0);
            player.setLevel(0);

            for (PotionEffect effect : player.getActivePotionEffects())
                player.removePotionEffect(effect.getType());

            player.getInventory().clear();
            player.getEquipment().clear();

            ItemStack guiSelectTeam = new ItemStack(Material.WOOL);
            ItemMeta meta = guiSelectTeam.getItemMeta();
            meta.setDisplayName("§eВыбор команды");
            guiSelectTeam.setItemMeta(meta);

            ItemStack hub = new ItemStack(Material.BED);
            meta = guiSelectTeam.getItemMeta();
            meta.setDisplayName("§eВернуться в лобби");
            hub.setItemMeta(meta);

            player.getInventory().setItem(4, guiSelectTeam);
            player.getInventory().setItem(8, hub);

            for (Profile all : arena.getProfiles().map().values()) {
                Player p = all.getPlayer();
                player.showPlayer(TowerRush.plugin(), p);
                p.showPlayer(TowerRush.plugin(), player);
            }

            if (arena.isWaiting() && arena.getArenaConfig().minPlayers == arena.getProfiles().size()) {
                arena.setState(GameState.STARTING);
            }
        }
    }

    @EventHandler
    public void onQuit(ArenaQuitEvent e) {
        GameArena arena = e.getArena();

        if (!arena.isGame()) {
            arena.broadcast(String.format("§f[§c§lKing og the Tower§f]: §e%s §7покинул игру.",
                    e.getProfile().getPlayer().getDisplayName()));
        }

        if (arena.isStarting() && !arena.isWaiting() && arena.getProfiles().size() < arena.getArenaConfig().minPlayers) {
            arena.setState(GameState.WAITING);
        }

        if (arena.isGame() && arena.getProfiles().size() == 0) {
            arena.reload();
        }
    }

    @EventHandler
    public void onSelectTeam(ArenaSelectTeamEvent e) {
        Profile profile = e.getProfile();
        Player player = profile.getPlayer();
        GameTeam team = e.getTeam();

        player.sendMessage(String.format(" §e%s §7присоединился к %s§7.",
                player.getName(), team.getColor() + team.getName()));

        ItemStack item = player.getInventory().getItem(4);
        item.setDurability((short) team.getByteCode());

        player.getInventory().setItem(4, item);
    }

    @EventHandler
    public void onChangeState(ArenaChangeStateEvent e) {
        GameState state = e.getState();
        GameState old = e.getOldState();
        GameArena arena = e.getArena();

        if (old != null && old.equals(GameState.STARTING)) {
            arena.getProfiles().map().values().stream().map(Profile::getPlayer).forEach(player -> {
                player.setTotalExperience(0);
                player.setExp(0);
            });
        }

        if (state.equals(GameState.WAITING)) {
            if (old.equals(GameState.STARTING))
                arena.getTimer().cancel();
            arena.time = -arena.getArenaConfig().timeToStart;
        } else if (state.equals(GameState.STARTING)) {
            BukkitTask timer = Bukkit.getScheduler().runTaskTimer(TowerRush.plugin(), () -> Bukkit.getPluginManager()
                    .callEvent(new ArenaTimerTickEvent(arena, arena.time++)), 20L, 20L);
            arena.setTimer(timer);
        } else if (state.equals(GameState.PLAYING)) {
            arena.broadcast("§f[§c§lKing of the Tower§f]: §aИгра началась, приятной игры!");
            arena.startingGame();
        } else if (state.equals(GameState.ENDING)) {
            Bukkit.getScheduler().runTaskLater(TowerRush.plugin(), ()-> {
                for (Profile profile : arena.getProfiles().map().values()) {
                    profile.getPlayer().teleport(arena.getArenaConfig().locLobby);
                    profile.toLobby();
                }
                arena.reload();
            }, 100L);
        }
    }

    @EventHandler
    public void onTimerTick(ArenaTimerTickEvent e) {
        GameArena arena = e.getArena();
        int time = e.getTime();
        if (time < 0) {
            int normaltime = Math.abs(time);
            arena.getProfiles().map().values().stream().map(Profile::getPlayer).forEach(player -> {
                player.setLevel(normaltime);
                player.setExp(normaltime / (float) arena.getArenaConfig().timeToStart);
            });
        } else if (arena.isStarting() && time == 0) {
            arena.setState(GameState.PLAYING);
        } else {
            List<GameBeacon> beacons = arena.getBeacons();
            if (time != 0 && time % 60 == 0) {
                List<BeaconAction> list = Arrays.stream(BeaconAction.values()).collect(Collectors.toList());
                Collections.shuffle(list);
                for (GameBeacon beacon : beacons)
                    beacon.setAction(list.remove(0));
            }
        }
    }

    @EventHandler
    public void onDamage(ArenaDamageEvent e) {
        GameArena arena = e.getArena();
        if (!arena.isGame() || arena.time < 5) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDeath(ArenaDeathEvent e) {
        Profile profile = e.getProfile();
        GameArena arena = e.getArena();
        Player player = profile.getPlayer();
        Location deathLoc = player.getLocation();
        profile.death();

        if (!e.isKilled()) {

            String msg;
            switch (e.getCause()) {
                case FALL:
                    msg = "%s §7не учел силы тяжести и разбился.";
                    break;
                case FALLING_BLOCK:
                    msg = "%s §7пришибло камнем.";
                    break;
                case FIRE:
                    msg = "%s §7превратился в шашлык.";
                    break;
                case BLOCK_EXPLOSION:
                    msg = "%s §7отправился в космос.";
                    break;
                case MAGIC:
                    msg = "%s §7убило магией.";
                    break;
                default:
                    msg = "%s §7помер.";
                    break;
            }

            arena.broadcast(String.format(" " + msg,
                    profile.getPlayer().getDisplayName()));
        }

        if (deathLoc.getBlockY() > 15) {

            World w = deathLoc.getWorld();
            Zombie zombie = w.spawn(deathLoc, Zombie.class);
            zombie.setCanPickupItems(false);
            zombie.setAI(false);

            ItemStack helmet = player.getEquipment().getHelmet();
            if (helmet != null)
                zombie.getEquipment().setChestplate(helmet);

            ItemStack chestplate = player.getEquipment().getChestplate();
            if (chestplate != null)
                zombie.getEquipment().setChestplate(chestplate);

            ItemStack leggings = player.getEquipment().getLeggings();
            if (leggings != null)
                zombie.getEquipment().setChestplate(leggings);

            ItemStack boots = player.getEquipment().getBoots();
            if (boots != null)
                zombie.getEquipment().setChestplate(boots);

            zombie.damage(200);
        }

        player.getInventory().clear();
        player.getEquipment().clear();
    }

    @EventHandler
    public void onFall(ArenaFallEvent e) {
        GameArena arena = e.getArena();
        Profile profile = e.getProfile();
        if (arena.isGame()) {
            profile.getPlayer().damage(200);
            profile.getPlayer().teleport(profile.getTeam().getSpawn());
        } else {
            profile.getPlayer().teleport(arena.getArenaConfig().locLobby);
            //if (profile.isAlive())
            //    profile.death();
        }
    }

    @EventHandler
    public void onKill(ArenaKillEvent e) {
        e.getArena().broadcast(String.format(" §e%s §7был убит §e%s§7.",
                e.getDead().getPlayer().getDisplayName(), e.getProfile().getPlayer().getDisplayName()));
    }

    @EventHandler
    public void onGoal(ArenaGoalEvent e) {
        Profile profile = e.getProfile();
        Player player = profile.getPlayer();
        GameTeam team = profile.getTeam();
        GameArena arena = profile.getArena();

        //getPlayer.startingGame(getTeam.getSpawn());
        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100, 2));
        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 80, 1));

        arena.broadcast(String.format("§f[§c§lKing of the Tower§f]: %s §7атаковал %s§7!",
                player.getDisplayName(), e.getAttackedTeam().getColor() + e.getAttackedTeam().getName()));
    }

    @EventHandler
    public void onLose(ArenaTeamLoseEvent e) {
        GameTeam team = e.getTeam();
        GameArena arena = team.getArena();

        arena.broadcast(String.format("§f[§c§lKing of the Tower§f]: %s §7проиграла!",
                team.getColor() + team.getName()));
        team.lose();
    }

    @EventHandler
    public void onWin(ArenaTeamWinEvent e) {
        GameTeam team = e.getTeam();
        GameArena arena = team.getArena();
        arena.setState(GameState.ENDING);

        arena.broadcast(String.format("§f[§c§lKing of the Tower§f]: §aИгра завершена, победила %s§a!",
                team.getColor() + team.getName()));
        //getTeam.lose();
    }

    @EventHandler
    public void onCaptureStage(ArenaCaptureStageEvent e) {
        GameTeam team = e.getTeam();
        Block b = e.getBlock();
        Location l = b.getLocation();
        World w = l.getWorld();

        for (float x = l.getBlockX(); x <= l.getBlockX() + 1; x += 0.2) {
            w.spawnParticle(Particle.BLOCK_CRACK, x, l.getBlockY() + 1.1, l.getBlockZ(), 1, 0, 0, 0, 0, team.getMaterialData());
            w.spawnParticle(Particle.BLOCK_CRACK, x, l.getBlockY() + 1.1, l.getBlockZ() + 1, 1, 0, 0, 0, 0, team.getMaterialData());
        }
        for (float z = l.getBlockZ(); z <= l.getBlockZ() + 1; z += 0.2) {
            w.spawnParticle(Particle.BLOCK_CRACK, l.getBlockX(), l.getBlockY() + 1.1, z, 1, 0, 0, 0, 0, team.getMaterialData());
            w.spawnParticle(Particle.BLOCK_CRACK, l.getBlockX() + 1, l.getBlockY() + 1.1, z, 1, 0, 0, 0, 0, team.getMaterialData());
        }
    }

    @EventHandler
    public void onCapture(ArenaCaptureEvent e) {
        GameTeam team = e.getTeam();
        GameArena arena = e.getArena();
        GameBeacon beacon = e.getBeacon();

        if (beacon.getAction() == null) {
            arena.broadcast(String.format(" §a%s §7захватила маяк!",
                    team.getColor() + team.getName()));
        } else {
            arena.broadcast(String.format(" §a%s §7захватила маяк с эффектом §8[§e%s§8]§7!",
                    team.getColor() + team.getName(), beacon.getAction().getDisplayName()));
        }
    }

    @EventHandler
    public void onBuyUpgrade(ArenaTeamBuyUpgradeEvent e) {
        Profile profile = e.getProfile();
        GameTeam team = e.getTeam();
        team.broadcast(String.format(" §a%s §7купил улучшение §8[%s§8]",
                profile.getPlayer().getDisplayName(), e.getTier().getItem().getItemMeta().getDisplayName()));

        if (e.getType().equals(UpgradeType.ORE_COUNT)) {
            GameOreSpawner gameOreSpawner = team.getOreSpawners().stream()
                    .filter(s -> !s.isActive)
                    .findFirst()
                    .orElse(null);
            if (gameOreSpawner != null)
                gameOreSpawner.activate();
        }
    }

    @EventHandler
    public void unloadCancaledTasks(ArenaTimerTickEvent e) {
        if (e.getTime() % 10 == 0) {
            GameArena arena = e.getArena();
            Iterator<BukkitTask> iter = arena.getTasks().iterator();
            while (iter.hasNext()) {
                if (iter.next().isCancelled())
                    iter.remove();
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockPlace(ArenaBlockPlaceEvent e) {
        GameArena arena = e.getArena();
        Block block = e.getBlock();
        boolean blocked = false;

        for (GameBeacon beacon : arena.getBeacons()) {
            if (distance(block, beacon.getLocation(), (int) (beacon.getRaduis() * beacon.getRaduis() * 1.7)))
                blocked = true;
        }

        for (GameTeam team : arena.getTeams().map().values()) {
            if (distance(block, team.getSpawn(), 16) || distance(block, team.getPortal(), 20) ||
                    distance(block, team.getLocShop(), 4) || distance(block, team.getLocUpgrader(), 4))
                blocked = true;
        }

        if (blocked) {
            e.setCancelled(true);
            e.getProfile().getPlayer().sendMessage(" §cЗдесь запрещено ставить блоки.");
        } else
            arena.getPlacedBlocks().add(block);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockBreak(ArenaBlockBreakEvent e) {
        Profile profile = e.getProfile();
        Player player = profile.getPlayer();
        Block b = e.getBlock();
        Material type = b.getType();
        e.getArena().getPlacedBlocks().remove(b);

        if (type.equals(Material.IRON_ORE)) {
            player.getInventory().addItem(new ItemStack(Material.IRON_INGOT, rnd.nextInt(3) + 1));
        } else if (type.equals(Material.GOLD_ORE)) {
            player.getInventory().addItem(new ItemStack(Material.GOLD_INGOT, rnd.nextInt(3) + 1));
        } else if (type.equals(Material.EMERALD_ORE)) {
            GameTeam team = profile.getTeam();
            player.getInventory().addItem(new ItemStack(Material.EMERALD, team.isActiveBeacon(BeaconAction.DOUBLE_EMERALDS) ? 2 : 1));
        }

        if (type.equals(Material.IRON_ORE) || type.equals(Material.GOLD_ORE) || type.equals(Material.EMERALD_ORE))
            player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1.3F, 0.15F);

    }

    @EventHandler
    public void onSpawnerTick(ArenaTimerTickEvent e) {
        if (e.getTime() > 0) {
            e.getArena().getTeams().map().values()
                    .stream()
                    .map(GameTeam::getOreSpawners)
                    .forEach(spawner -> spawner.stream()
                            .filter(GameOreSpawner::isActive)
                            .forEach(GameOreSpawner::tick));
        }
    }

    @EventHandler
    public void onSpawner(ArenaOreSpawnerEvent e) {
        GameArena arena = e.getArena();
        GameTeam team = e.getTeam();
        int level = arena.getUpgradeCore().getLevel(team, UpgradeType.ORE_QUALITY);
        int r = rnd.nextInt(100);
        Material m;
        if (level == 0) {
            if (r < 3) m = Material.EMERALD_ORE;
            else if (r < 15) m = Material.GOLD_ORE;
            else if (r < 40) m = Material.IRON_ORE;
            else m = Material.STONE;
        } else if (level == 1) {
            if (r < 5) m = Material.EMERALD_ORE;
            else if (r < 30) m = Material.GOLD_ORE;
            else if (r < 70) m = Material.IRON_ORE;
            else m = Material.STONE;
        } else if (level == 2) {
            if (r < 7) m = Material.EMERALD_ORE;
            else if (r < 35) m = Material.GOLD_ORE;
            else if (r < 85) m = Material.IRON_ORE;
            else m = Material.STONE;
        } else {
            if (r < 10) m = Material.EMERALD_ORE;
            else if (r < 40) m = Material.GOLD_ORE;
            else if (r < 95) m = Material.IRON_ORE;
            else m = Material.STONE;
        }
        e.setType(m);
        arena.getPlacedBlocks().add(e.getBlock());
    }

    @EventHandler
    public void onRespawnPlayer(ArenaRespawnEvent e) {
        Profile profile = e.getProfile();
        Player player = profile.getPlayer();

        ItemStack chest = itemChestPlate.clone();
        LeatherArmorMeta meta = (LeatherArmorMeta) chest.getItemMeta();
        meta.setColor(BukkitColor.getByCode(profile.getTeam().getByteCode()).getColor());
        chest.setItemMeta(meta);

        player.getInventory().addItem(itemPickAxe);
        player.getInventory().addItem(itemAxe);

        if (e.getArena().time <= 1) {
            player.getInventory().setItem(7, new ItemStack(Material.IRON_INGOT, 12));
            player.getInventory().setItem(8, new ItemStack(Material.GOLD_INGOT, 2));
        }

        player.getEquipment().setChestplate(chest);
    }

    @EventHandler
    public void onChangeAction(ArenaActionChangeEvent e) {
        GameArena arena = e.getArena();
        if (e.getOldAction() != null) {

            GameTeam team = e.getBeacon().getTeam();

            arena.broadcast(String.format(" §7Перевыбор: §8[%s■§8] §e%s §b➦ §6%s.", team == null ? "§7" : team.getColor(),
                    e.getOldAction().getDisplayName(), e.getAction().getDisplayName()));
        }
    }

    private boolean distance(Block block, Location location, int radius) {
        Location l = block.getLocation();
        double distance = Math.pow(l.getBlockX() - location.getBlockX(), 2) + Math.pow(l.getBlockY() - location.getBlockY(), 2) / 9 + Math.pow(l.getBlockZ() - location.getBlockZ(), 2);
        return distance <= radius + 1;
    }
}
