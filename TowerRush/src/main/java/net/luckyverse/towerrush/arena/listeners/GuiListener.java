package net.luckyverse.towerrush.arena.listeners;

import net.luckyverse.towerrush.TowerRush;
import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.arena.beacons.GameBeacon;
import net.luckyverse.towerrush.profile.Profile;
import net.luckyverse.towerrush.shops.upgrade.UpgradableItem;
import net.luckyverse.towerrush.shops.upgrade.UpgradeSystemCore;
import net.luckyverse.towerrush.shops.upgrade.UpgradeableTier;
import net.luckyverse.towerrush.stores.ArenaStore;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.yooxa.connecter.Core;

import java.util.*;

public class GuiListener implements Listener {

    private static final Set<Inventory> guiSelectTeam = new HashSet<>();
    private static final Map<Inventory, Profile> guiUpgradesMenu = new HashMap<>();
    private ArenaStore store;
    private Set<Player> set = new HashSet<>();

    public GuiListener(ArenaStore store) {
        this.store = store;
    }

    public static void addSelectTeam(Inventory inv) {
        guiSelectTeam.add(inv);
    }

    public static void addUpgradesMenu(Inventory inv, Profile profile) {
        guiUpgradesMenu.put(inv, profile);
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Inventory inv = e.getInventory();
        ItemStack item = e.getCurrentItem();
        Player player = (Player) e.getWhoClicked();
        if (item != null && (inv.getType().equals(InventoryType.CHEST) || inv.getType().equals(InventoryType.HOPPER))) {

            boolean isSelectTeam = guiSelectTeam.contains(inv);
            boolean isUpgradesMenu = guiUpgradesMenu.containsKey(inv);

            if (isSelectTeam || isUpgradesMenu) {
                e.setCancelled(true);

                Profile profile = store.findProfile(player);
                GameArena arena = profile.getArena();

                if (arena == null || profile == null) {
                    player.closeInventory();
                    return;
                }

                if (isSelectTeam) {

                    if (arena.isGame()) {
                        player.closeInventory();
                        return;
                    }

                    GameTeam selectTeam = arena.getTeams().get((int) item.getDurability());
                    if (selectTeam.getMembers().contains(profile)) {
                        player.sendMessage("§f[§c§lTowerRush§f]: §cВы уже в этой команде.");
                    } else if (!selectTeam.isFree()) {
                        player.sendMessage("§f[§c§lTowerRush§f]: §cВ этой команде уже достаточно игроков.");
                    } else {
                        if (profile.getTeam() != null)
                            profile.getTeam().quit(profile);
                        selectTeam.join(profile);
                        arena.getTeams().openInventorySelectTeam(profile);
                    }

                } else if (isUpgradesMenu) {

                    UpgradeSystemCore menu = arena.getUpgradeCore();
                    UpgradableItem upgrade = menu.getUpgradeBySlot(e.getSlot());
                    if (upgrade == null)
                        return;
                    UpgradeableTier tier = upgrade.getTier(profile.getTeam());

                    Inventory playerInv = player.getInventory();

                    if (item.getType().equals(Material.AIR)) {
                        return;
                    }

                    if (tier == null) {
                        player.closeInventory();
                        return;
                    }

                    if (playerInv.contains(Material.IRON_INGOT, tier.getIron()) &&
                            playerInv.contains(Material.GOLD_INGOT, tier.getGold()) &&
                            playerInv.contains(Material.EMERALD, tier.getEmerald())) {

                        playerInv.removeItem(new ItemStack(Material.IRON_INGOT, tier.getIron()),
                                new ItemStack(Material.GOLD_INGOT, tier.getGold()),
                                new ItemStack(Material.EMERALD, tier.getEmerald()));

                        menu.buy(profile, upgrade.getType(), tier);

                        Iterator<Profile> iter = guiUpgradesMenu.values().iterator();
                        while (iter.hasNext()) {
                            Profile member = iter.next();
                            if (profile.getTeam().equals(member.getTeam())) {
                                //iter.remove();
                                menu.open(member);
                            }
                        }

                    } else {
                        player.sendMessage("§f[§c§lTowerRush§f]: §cНедостаточно средств для покупки улучшения!");
                        return;
                    }
                }
            }
        }
    }

    @EventHandler
    public void onClick(PlayerInteractEntityEvent e) {
        Entity entity = e.getRightClicked();
        if (entity.getType().equals(EntityType.VILLAGER)) {
            e.setCancelled(true);

            Player player = e.getPlayer();
            Profile profile = store.findProfile(player);
            if (profile != null && profile.isAlive()) {

                GameTeam team = profile.getTeam();
                if (entity.hasMetadata("team") && entity.getMetadata("team").get(0).asInt() == team.getByteCode()) {

                    if (entity.hasMetadata("type")) {
                        String type = entity.getMetadata("type").get(0).asString();
                        if (type.equals("shop")) {
                            profile.getArena().getShopCore().open(profile);
                            return;
                        } else if (type.equals("upgrader")) {
                            profile.getArena().getUpgradeCore().open(profile);
                            return;
                        }
                    }

                } else {
                    player.sendMessage("§cЭто торговец не вашей команды!");
                    return;
                }
            }
        }
    }

    @EventHandler
    public void onClick(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            Player player = e.getPlayer();

            GameArena arena = store.map().values().stream()
                    .filter(a -> a.getProfiles().get(player) != null)
                    .findFirst()
                    .orElse(null);

            Block block = e.getClickedBlock();

            if (arena != null && arena.isStarting()) {
                e.setCancelled(true);
                Profile profile = arena.getProfiles().get(player);

                ItemStack item = e.getItem();
                if (item == null)
                    return;

                if (item.getType().equals(Material.WOOL))
                    arena.getTeams().openInventorySelectTeam(profile);

                if (item.getType().equals(Material.BED))
                    profile.toLobby();
            } else if (arena != null && block != null && e.getClickedBlock().getType().equals(Material.SKULL)
                    && !set.contains(player)) {

                e.setCancelled(true);

                set.add(player);
                Bukkit.getScheduler().runTaskLater(TowerRush.plugin(), ()-> set.remove(player), 20L);

                GameBeacon beacon = null;
                for (GameBeacon b : arena.getBeacons()) {
                    Location locBeacon = b.getLocation();
                    if (locBeacon.getWorld().equals(block.getWorld()) && locBeacon.getBlockX() == block.getX() &&
                            locBeacon.getBlockZ() == block.getZ()) {

                        beacon = b;
                        break;
                    }
                }

                if (beacon != null)
                    beacon.sendInformation(player);
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        guiSelectTeam.remove(e.getInventory());
        guiUpgradesMenu.remove(e.getInventory());
    }
}
