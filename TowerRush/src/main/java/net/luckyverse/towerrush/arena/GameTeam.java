package net.luckyverse.towerrush.arena;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.luckyverse.sugarboy.StringMetadata;
import net.luckyverse.sugarboy.cosmetics.Holograms;
import net.luckyverse.towerrush.TowerRush;
import net.luckyverse.towerrush.arena.beacons.BeaconAction;
import net.luckyverse.towerrush.arena.beacons.GameBeacon;
import net.luckyverse.towerrush.events.player.ArenaSelectTeamEvent;
import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GameTeam {

    @Expose
    @SerializedName("max-players")
    private int limit = 1;

    @Expose
    private String name = "Красная команда";

    @Expose
    @SerializedName("color-chars")
    private String colorCode = "§c";

    @Expose
    private Location spawn = new Location(Bukkit.getWorld("world"), 0, 0, 0);

    @Expose
    @SerializedName("central-portal-block")
    private Location portal = new Location(Bukkit.getWorld("world"), 0, 0, 0);

    @Expose
    @SerializedName("byte-code")
    private int byteCode = 14;

    @Expose
    @SerializedName("villager-upgrade")
    private Location locUpgrader = new Location(Bukkit.getWorld("world"), 0, 0, 0);

    @Expose
    @SerializedName("villager-shop")
    private Location locShop = new Location(Bukkit.getWorld("world"), 0, 0, 0);

    @Expose
    @SerializedName("spawners")
    private Set<GameOreSpawner> oreSpawners = new HashSet<>();

    private int health;
    private List<Profile> members = new ArrayList<>();
    private Set<Block> portals = new HashSet<>();
    private GameArena arena;
    private boolean isLose = false;
    private boolean canDamaged = true;
    private MaterialData materialData;

    public void init(GameArena arena) {
        this.arena = arena;
        this.health = arena.getArenaConfig().winHealth / arena.getArenaConfig().teams.size();
        arena.getTeams().add(byteCode, this);
        Holograms.setText("portal-" + arena.ARENA_NAME + "-" + name, portal.add(0, .5, 0),
                colorCode + name + ":", healthBar());

        for (int x = portal.getBlockX() - 3; x < portal.getBlockX() + 4; x++) {
            for (int z = portal.getBlockZ() - 3; z < portal.getBlockZ() + 4; z++) {
                Block block = portal.getWorld().getBlockAt(x, portal.getBlockY(), z);
                if (block.getType().equals(Material.ENDER_PORTAL))
                    portals.add(block);
            }
        }

        oreSpawners.forEach(spawner -> spawner.init(this));

        World world = Bukkit.getWorld(locUpgrader.getWorld().getName());

        Villager upgrader = world.spawn(locUpgrader, Villager.class);
        upgrader.setAI(false);
        upgrader.setCustomName("§e§lПродавец улучшений");
        upgrader.setCustomNameVisible(true);
        upgrader.setMetadata("team", new StringMetadata(byteCode));
        upgrader.setMetadata("type", new StringMetadata("upgrader"));
        upgrader.setProfession(Villager.Profession.BUTCHER);

        Villager shop = world.spawn(locShop, Villager.class);
        shop.setAI(false);
        shop.setCustomName("§e§lПродавец предметов");
        shop.setCustomNameVisible(true);
        shop.setMetadata("team", new StringMetadata(byteCode));
        shop.setMetadata("type", new StringMetadata("shop"));
        upgrader.setProfession(Villager.Profession.LIBRARIAN);

        this.materialData = new MaterialData(Material.WOOL, (byte) byteCode);
    }

    public void join(Profile profile) {
        members.add(profile);
        profile.setTeam(this);
        Player player = profile.getPlayer();
        player.setDisplayName(colorCode + player.getName());
        player.setPlayerListName(colorCode + player.getName());
        Bukkit.getPluginManager().callEvent(new ArenaSelectTeamEvent(profile.getArena(), profile, this));
    }

    public void quit(Profile profile) {
        members.remove(profile);
        profile.setTeam(null);
    }

    public void broadcast(String msg) {
        members.stream().map(Profile::getPlayer).forEach(p -> p.sendMessage(msg));
    }
    
    public void lose() {
        isLose = true;
        for (Profile profile : members) {
            Player player = profile.getPlayer();
            arena.getProfiles().remove(player);
            player.setGameMode(GameMode.SPECTATOR);
            profile.setTeam(null);

            Bukkit.getScheduler().runTaskLater(TowerRush.plugin(), ()-> profile.toLobby(), 100L);
        }
        members.clear();
    }

    public boolean removeHealth() {
        if (health > 0) {
            this.health--;
            canDamaged = false;
            updateHolo();
            setBlockPortal(Material.AIR);

            if (health != 0) {
                Bukkit.getScheduler().runTaskLater(TowerRush.plugin(), () -> {
                    setBlockPortal(Material.ENDER_PORTAL);
                    canDamaged = true;
                }, 100L);
            }
        }
        return health == 0;
    }

    public boolean addHealth() {
        this.health++;
        updateHolo();
        return health == arena.getArenaConfig().winHealth;
    }

    public int getHealth() {
        return health;
    }

    public int getCount() {
        return members.size();
    }

    public int getLimit() {
        return limit;
    }

    public int getByteCode() {
        return byteCode;
    }

    public int distanceTo(Location location) {
        return (int) portal.distance(location);
    }

    public boolean isFree() {
        return members.size() < limit;
    }

    public boolean isLose() {
        return isLose;
    }

    public boolean isCanDamaged() {
        return canDamaged;
    }

    public boolean isActiveBeacon(BeaconAction action) {
        for (GameBeacon beacon : arena.getBeacons())
            if (this.equals(beacon.getTeam()) && action.equals(beacon.getAction()))
                return true;
        return false;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return colorCode;
    }

    public List<Profile> getMembers() {
        return members;
    }

    public Set<Block> getPortalBlocks() {
        return portals;
    }

    public Set<GameOreSpawner> getOreSpawners() {
        return oreSpawners;
    }

    public MaterialData getMaterialData() {
        return materialData;
    }

    public Location getSpawn() {
        return spawn;
    }

    public Location getPortal() {
        return portal;
    }

    public Location getLocUpgrader() {
        return locUpgrader;
    }

    public Location getLocShop() {
        return locShop;
    }

    public GameArena getArena() {
        return arena;
    }

    public String toString() {
        String info = String.format("%s§7[§e%d§7/§e%d§7]:",
                colorCode + name,
                members.size(),
                limit);
        for (Profile profile : members)
            info += "§r\n   - " + profile.getPlayer().getDisplayName();

        return info;
    }

    private void updateHolo() {
        Holograms.editText("portal-" + arena.ARENA_NAME + "-" + name, 2, healthBar());
    }

    private void setBlockPortal(Material material) {
        portals.forEach(b -> b.setType(material));
    }

    public String healthBar() {
        String bar;
        if (health != 0) {
            bar = "§8[";
            for (int q = 0; q < arena.getArenaConfig().winHealth; q++)
                bar += (q < health ? colorCode : "§7") + "❤";
            bar += "§8]";
        } else bar = "§4§l✕ ✕ ✕ ✕ ✕ ✕ ✕";
        return bar;
    }
}