package net.luckyverse.towerrush.arena.listeners;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.arena.beacons.BeaconAction;
import net.luckyverse.towerrush.arena.beacons.GameBeacon;
import net.luckyverse.towerrush.events.ArenaTimerTickEvent;
import net.luckyverse.towerrush.events.player.ArenaDamageByEnemyEvent;
import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BeaconListener implements Listener {

    private final Map<GameBeacon, Set<BukkitTask>> tasks = new HashMap<>();

    private Set<BukkitTask> getTasks(GameBeacon beacon) {
        if (tasks.containsKey(beacon)) {
            return tasks.get(beacon);
        } else {
            Set<BukkitTask> tasks = new HashSet<>();
            this.tasks.put(beacon, tasks);
            return tasks;
        }
    }

    private Set<BukkitTask> getTasksAndStop(GameBeacon beacon) {
        Set<BukkitTask> tasks = getTasks(beacon);
        for (BukkitTask task : tasks)
            if (!task.isCancelled())
                task.cancel();
        tasks.clear();
        return tasks;
    }

    @EventHandler
    public void onTick(ArenaTimerTickEvent e) {
        GameArena arena = e.getArena();

        for (GameBeacon beacon : arena.getBeacons()) {
            BeaconAction action = beacon.getAction();
            GameTeam team = beacon.getTeam();

            if (action == null || team == null)
                continue;

            if (action.equals(BeaconAction.SPEED)) {
                PotionEffect effect = new PotionEffect(PotionEffectType.SPEED, 100, 0);
                team.getMembers().stream()
                        .map(Profile::getPlayer)
                        .forEach(p -> p.addPotionEffect(effect));
            } else if (action.equals(BeaconAction.JUMPING)) {
                PotionEffect effect = new PotionEffect(PotionEffectType.JUMP, 100, 0);
                team.getMembers().stream()
                        .map(Profile::getPlayer)
                        .forEach(p -> p.addPotionEffect(effect));
            }
        }
    }

    @EventHandler
    public void onDamage(ArenaDamageByEnemyEvent e) {
        Profile enemy = e.getEnemy();
        GameTeam team = enemy.getTeam();
        if (team.isActiveBeacon(BeaconAction.LIFESTEAL) &&
                e.getBasic().getDamager().getType().equals(EntityType.PLAYER)) {

            Player p = enemy.getPlayer();
            double health = p.getHealth() + e.getBasic().getFinalDamage() * 0.15D;
            p.setHealth(health > 20 ? 20 : health);
        }
    }
}
