package net.luckyverse.towerrush.arena;

import net.luckyverse.sugarboy.files.MyFileUtils;
import net.luckyverse.towerrush.TowerRush;
import net.luckyverse.towerrush.arena.beacons.GameBeacon;
import net.luckyverse.towerrush.events.ArenaChangeStateEvent;
import net.luckyverse.towerrush.events.player.ArenaJoinEvent;
import net.luckyverse.towerrush.events.player.ArenaQuitEvent;
import net.luckyverse.towerrush.profile.Profile;
import net.luckyverse.towerrush.shops.ShopSystemCore;
import net.luckyverse.towerrush.shops.upgrade.UpgradeSystemCore;
import net.luckyverse.towerrush.stores.ProfileStore;
import net.luckyverse.towerrush.stores.TeamStore;
import net.luckyverse.towerrush.utils.configurations.ArenaSettings;
import net.luckyverse.towerrush.utils.configurations.GameBoard;
import net.luckyverse.towerrush.utils.configurations.StartingBoard;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class GameArena {

    public final String ARENA_NAME;

    public int time;
    private ArenaSettings config;

    private TeamStore teams = new TeamStore();
    private ProfileStore profiles = new ProfileStore();
    private List<GameBeacon> beacons = new ArrayList<>();
    private List<BukkitTask> tasks = new ArrayList<>();
    private Set<Block> placedBlocks = new HashSet<>();

    private GameState state;

    private World world;

    private UpgradeSystemCore upgradeCore;
    private ShopSystemCore shopCore;

    private BukkitTask timer;
    private StartingBoard startingBoard;


    public GameArena(ArenaSettings settings) {
        this.ARENA_NAME = settings.name;
        this.config = settings;
    }

    public void load(boolean copyScheme) {
        TowerRush.logger().info(GameArena.class,
                String.format("Начинаем процесс загрузки карты для арены %s.", config.world));
        if (copyScheme)
            MyFileUtils.copySchemWorld(TowerRush.plugin(), "worlds", config.world);
        world = TowerRush.plugin().getServer().createWorld(new WorldCreator(config.world));

        for (ArenaSettings settings : TowerRush.plugin().reloadConfigs().arenaSettings)
            if (settings.name.equals(ARENA_NAME))
                this.config = settings;

        setState(GameState.WAITING);
        this.time = -config.timeToStart;

        world.setThundering(false);
        world.setStorm(false);
        world.setThunderDuration(Integer.MAX_VALUE);
        world.setGameRuleValue("doDaylightCycle", "false");
        world.setGameRuleValue("announceAdvancements", "false");
        world.setGameRuleValue("doWeatherCycle", "false");
        world.setGameRuleValue("randomTickSpeed", "0");
        world.setAutoSave(false);
        world.setSpawnLocation(config.locLobby);

        config.teams.stream().forEach(team -> team.init(this));
        config.beacons.stream().forEach(beacon -> beacon.init(this));

        this.upgradeCore = new UpgradeSystemCore(this);
        this.shopCore = new ShopSystemCore(upgradeCore);
        this.startingBoard = new StartingBoard(this);

    }

    public void unload() {
        if (!isRestarting())
            setState(GameState.RESTARTING);

        for (Player p : world.getPlayers())
            p.kickPlayer("Перезагрузка арены.");

        for (BukkitTask task : tasks)
            task.cancel();

        profiles.map().clear();
        teams.map().clear();
        beacons.clear();
        placedBlocks.clear();

        for (BukkitTask task : tasks)
            if (!task.isCancelled())
                task.cancel();
        tasks.clear();

        if (timer != null)
            timer.cancel();
        timer = null;

        Bukkit.getServer().unloadWorld(world, true);
        TowerRush.logger().info(GameArena.class, String.format("Мир %s был выгружен", world.getName()));
        world = null;
    }

    public void reload() {
        setState(GameState.RESTARTING);
        Bukkit.getScheduler().runTaskLater(TowerRush.plugin(), () -> {
            unload();
            load(true);
        }, 100L);
    }

    public void join(Player player) {
        Profile profile = new Profile(player, this);
        profiles.add(player, profile);
        player.setScoreboard(startingBoard.getScoreboard());
        Bukkit.getPluginManager().callEvent(new ArenaJoinEvent(this, profile));
    }

    public void quit(Player player) {
        Profile profile = profiles.get(player);
        if (profile != null) {
            profiles.remove(player);
            teams.map().values().forEach(t -> t.quit(profile));
            Bukkit.getPluginManager().callEvent(new ArenaQuitEvent(this, profile));
        }
    }

    public void setState(GameState state) {
        Bukkit.getPluginManager().callEvent(new ArenaChangeStateEvent(this, this.state, state));
        this.state = state;
    }

    public void startingGame() {
        GameBoard board = new GameBoard(this);
        board.setForAll(profiles.map().values().stream().map(Profile::getPlayer).collect(Collectors.toList()));

        for (Profile profile : profiles.map().values()) {
            if (profile.getTeam() == null)
                teams.joinInRandom(profile);
            profile.spawn();
        }
    }

    public void broadcast(String msg) {
        profiles.map().values().stream()
                .forEach(p -> p.getPlayer().sendMessage(msg));
    }

    public void addTask(BukkitTask task) {
        tasks.add(task);
    }

    public void setTimer(BukkitTask timer) {
        addTask(timer);
        this.timer = timer;
    }

    public BukkitTask getTimer() {
        return timer;
    }

    public TeamStore getTeams() {
        return teams;
    }

    public List<GameBeacon> getBeacons() {
        return beacons;
    }

    public List<BukkitTask> getTasks() {
        return tasks;
    }

    public Set<Block> getPlacedBlocks() {
        return placedBlocks;
    }

    public ArenaSettings getArenaConfig() {
        return config;
    }

    public ProfileStore getProfiles() {
        return profiles;
    }

    public UpgradeSystemCore getUpgradeCore() {
        return upgradeCore;
    }

    public ShopSystemCore getShopCore() {
        return shopCore;
    }

    public int getLimit() {
        return teams.size() * teams.map().values().iterator().next().getLimit();
    }

    public boolean isStarting() {
        return state.equals(GameState.WAITING) || state.equals(GameState.STARTING);
    }

    public boolean isGame() {
        return state.equals(GameState.PLAYING);
    }

    public boolean isWaiting() {
        return state.equals(GameState.WAITING);
    }

    public boolean isRestarting() {
        return state.equals(GameState.RESTARTING);
    }

    public String toString() {
        String info = String.format("§7Арена §e%s§7: Карта: §e%s §7Игроков: §e%d§7/§e%d §7Время §e%d §7Идет игра: §e%b §7Команды:",
                config.world,
                config.map,
                profiles.size(),
                getLimit(),
                time,
                isGame());
        for (GameTeam team : teams.map().values())
            info += "§r\n - " + team.toString();

        return info;
    }
}