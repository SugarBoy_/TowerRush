package net.luckyverse.towerrush.arena.beacons;

import com.google.gson.annotations.Expose;
import net.luckyverse.towerrush.TowerRush;
import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.events.beacon.ArenaActionChangeEvent;
import net.luckyverse.towerrush.events.beacon.ArenaCaptureEvent;
import net.luckyverse.towerrush.events.beacon.ArenaCaptureStageEvent;
import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class GameBeacon {

    private static final Random rnd = new Random();

    @Expose
    private Location location;

    @Expose
    private int raduis = 3;

    private Set<Block> wools = new HashSet<>();
    private GameArena arena;
    private GameTeam owner;
    private Set<GameTeam> lastForwards = new HashSet<>();
    private BeaconAction action;

    public void init(GameArena arena) {
        this.arena = arena;

        location = location.add(0, -1, 0);
        World world = location.getWorld();

        if (!world.getBlockAt(location).getType().equals(Material.STAINED_GLASS))
            return;

        arena.getBeacons().add(this);

        for (int x = location.getBlockX() - 5; x <= location.getBlockX() + 5; x++) {
            for (int z = location.getBlockZ() - 5; z <= location.getBlockZ() + 5; z++) {
                Block block = world.getBlockAt(x, location.getBlockY(), z);
                if (block.getType().equals(Material.WOOL))
                    wools.add(block);
            }
        }

        long timetick = (long) (200D / wools.size() + 0.99D);
        arena.addTask(Bukkit.getScheduler().runTaskTimer(TowerRush.plugin(), () -> {
            if (arena.isGame())
                update();
        }, 20L, timetick));
        TowerRush.logger().info(GameArena.class, "Маяк был инициализирован.");
    }

    public void sendInformation(Player player) {
        player.sendMessage("§8§m------------------------------------------");
        if (arena.time < 60) {
            player.sendMessage(String.format("   §7Активация: §e%s сек", 60 - arena.time));
        } else if (action == null) {
            player.sendMessage("   §7Происходит активация");
        } else {
            player.sendMessage(String.format("   §7Текущий эффект: §e%s", action.getDisplayName()));
            player.sendMessage(String.format("   §7Перевыбор: §e%d сек", 60 - arena.time % 60 + 1));

        }
        player.sendMessage("§8§m------------------------------------------");
    }

    private void update() {
        lastForwards.clear();
        for (Profile profile : arena.getProfiles().map().values()) {
            if (profile.isAlive()) {
                Player player = profile.getPlayer();
                Location l = player.getLocation();
                double distance = Math.pow(l.getBlockX() - location.getBlockX(), 2) + Math.pow(l.getBlockZ() - location.getBlockZ(), 2);
                if (player.getWorld().equals(location.getWorld()) && distance <= raduis * raduis &&
                        Math.abs(location.getBlockY() - l.getBlockY()) < 5) {

                    lastForwards.add(profile.getTeam());
                }
            }
        }

        if (lastForwards.size() > 1)
            return;

        GameTeam team = lastForwards.size() == 0 ? owner : lastForwards.stream().findFirst().get();
        byte code = (byte) (team == null ? 0 : team.getByteCode());

        List<Block> list = wools.stream()
                .filter(b -> b.getData() != code)
                .collect(Collectors.toList());

        if (list.size() != 0 && (lastForwards.size() == 1 || rnd.nextInt(3) == 0)) {
            Block block = list.get(rnd.nextInt(list.size()));
            block.setData(code);
            if (lastForwards.size() == 1) {
                Bukkit.getPluginManager().callEvent(new ArenaCaptureStageEvent(arena, this, team, block));
                if (list.size() == 1 && team != null && !team.equals(owner)) {
                    Bukkit.getPluginManager().callEvent(new ArenaCaptureEvent(arena, this, team));
                    capture(team);
                }
            }
        }
    }

    public void capture(GameTeam team) {
        owner = team;
        location.getWorld().getBlockAt(location).setData((byte) team.getByteCode());
    }

    public Location getLocation() {
        return location;
    }

    public int getRaduis() {
        return raduis;
    }

    public GameTeam getTeam() {
        return owner;
    }

    public BeaconAction getAction() {
        return action;
    }

    public void setAction(BeaconAction action) {
        Bukkit.getPluginManager().callEvent(new ArenaActionChangeEvent(arena, this, this.action, action));
        this.action = action;
    }
}
