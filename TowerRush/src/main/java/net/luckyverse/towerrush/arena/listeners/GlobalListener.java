package net.luckyverse.towerrush.arena.listeners;

import net.luckyverse.towerrush.TowerRush;
import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.events.player.*;
import net.luckyverse.towerrush.events.team.ArenaTeamLoseEvent;
import net.luckyverse.towerrush.events.team.ArenaTeamWinEvent;
import net.luckyverse.towerrush.profile.Profile;
import net.luckyverse.towerrush.stores.ArenaStore;
import net.luckyverse.towerrush.utils.configurations.GlobalSettings;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitTask;
import ru.yooxa.connecter.Core;
import ru.yooxa.connecter.events.CoreMessageEvent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GlobalListener implements Listener {

    private static final Set<Block> fallings = new HashSet<>();
    private static final Map<String, String> coreJoins = new HashMap<>();

    private ArenaStore store;

    public GlobalListener(ArenaStore store, GlobalSettings config) {
        this.store = store;
    }

    @EventHandler
    public void onCore(CoreMessageEvent e) {
        if (e.getTag().equals("changearena")) {
            String[] data = e.getMessage().split(" ");
            coreJoins.put(data[1], data[0]);
            TowerRush.logger().info(Core.class, String.format("Игрок %s будет перенаправлен на арену %s", data[1], data[0]));
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);

        Player player = e.getPlayer();
        String server = coreJoins.get(player.getName());

        System.out.println(coreJoins);
        if (server != null) {
            coreJoins.remove(player.getName());

            GameArena arena = store.getByName(server);
            if (arena != null && arena.isStarting() && arena.getProfiles().size() < arena.getLimit()) {
                arena.join(player);
                return;
            }
        }

        for (int q = 0; q < 25; q++)
            player.sendMessage(" ");

        player.sendMessage("§f[§c§lKing og the Tower§f]: §cНе удалось подключиться к серверу, возможные причины этому:");
        player.sendMessage("§7   - Вы подключались не через лобби King of The Tower");
        player.sendMessage("§7   - Вы находитесь в группе (Party)");
        player.sendMessage("");

        Bukkit.getScheduler().runTaskLater(TowerRush.plugin(), ()->
                Core.redirect(player, "KoT-Lobby-1"), 20L);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        store.map().values().forEach(a-> a.quit(player));
        e.setQuitMessage(null);
    }

    @EventHandler
    public void onCraft(CraftItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        Entity entity = e.getEntity();
        if (entity.getType().equals(EntityType.PLAYER)) {
            Player player = (Player) entity;
            Profile profile = store.findProfile(player);
            if (profile != null && profile.isAlive()) {
                Bukkit.getPluginManager().callEvent(new ArenaDamageEvent(profile, e));
                if (!e.isCancelled() && player.getHealth() - e.getFinalDamage() <= 0) {
                    e.setCancelled(true);
                    Bukkit.getPluginManager().callEvent(new ArenaDeathEvent(profile, e.getCause(),
                            e.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK) ||
                                    e.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_SWEEP_ATTACK) ||
                                    e.getCause().equals(EntityDamageEvent.DamageCause.PROJECTILE)));
                }
            }
        } else if (entity.getType().equals(EntityType.VILLAGER)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        Entity entity = e.getEntity();
        if (entity.getType().equals(EntityType.PLAYER)) {

            Player player = (Player) entity;
            Profile profile = store.findProfile(player);
            Entity entityDamager = e.getDamager();
            Profile damager = null;

            if (entityDamager.getType().equals(EntityType.PLAYER)) {

                Player playerDamager = (Player) entityDamager;
                damager = store.findProfile(playerDamager);
                if (damager != null && profile != null && damager.isAlive() && profile.isAlive() &&
                        !profile.getTeam().equals(damager.getTeam())) {
                    Bukkit.getPluginManager().callEvent(new ArenaDamageByEnemyEvent(profile, damager, e));
                } else e.setCancelled(true);

            } else if (entityDamager.getType().equals(EntityType.ARROW)) {

                Projectile projectile = (Projectile) entityDamager;
                ProjectileSource source = projectile.getShooter();
                if (source instanceof Player) {
                    Player playerDamager = (Player) source;
                    damager = store.findProfile(playerDamager);
                    if (damager != null && profile != null && damager.isAlive() && profile.isAlive() &&
                            !damager.equals(profile) && !profile.getTeam().equals(damager.getTeam())) {
                        profile.setLastDamager(damager);
                        Bukkit.getPluginManager().callEvent(new ArenaArrowHitEvent(profile, damager, projectile, e));
                    } else e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getWhoClicked().getGameMode().equals(GameMode.ADVENTURE) || e.getSlot() == 38)
            e.setCancelled(true);
    }

    @EventHandler
    public void onClick(PlayerDropItemEvent e) {
        if (e.getPlayer().getGameMode().equals(GameMode.ADVENTURE))
            e.setCancelled(true);
    }

    @EventHandler
    public void onSpawnPrevent(CreatureSpawnEvent e) {
        if (!e.getEntityType().equals(EntityType.PLAYER) && !e.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.CUSTOM))
            e.setCancelled(true);
    }

    @EventHandler
    public void onChangeFoodLevel(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onFall(PlayerMoveEvent e) {
        if (e.getTo().getBlockY() < 0) {
            Profile profile = store.findProfile(e.getPlayer());
            if (profile != null)
                Bukkit.getPluginManager().callEvent(new ArenaFallEvent(profile));
        }

        World world = e.getFrom().getWorld();
        Block block = world.getBlockAt(e.getFrom());
        if (block.getType().equals(Material.ENDER_PORTAL)) {

            Player player = e.getPlayer();
            player.stopSound(Sound.BLOCK_PORTAL_TRAVEL);

            Profile profile = store.findProfile(player);
            if (profile == null)
                return;

            BukkitTask task = Bukkit.getScheduler().runTaskTimer(TowerRush.plugin(), () -> Bukkit.getOnlinePlayers()
                    .forEach(p -> p.stopSound(Sound.BLOCK_PORTAL_TRAVEL)), 0L, 1L);
            Bukkit.getScheduler().runTaskLater(TowerRush.plugin(), ()-> task.cancel(), 20L);
            GameArena arena = profile.getArena();

            if (arena != null && arena.isGame()) {
                GameTeam attackedTeam = null;
                int distance = 100000;
                for (GameTeam t : arena.getTeams().map().values()) {
                    int d = t.distanceTo(player.getLocation());
                    if (d < distance) {
                        distance = d;
                        attackedTeam = t;
                    }
                }

                GameTeam team = profile.getTeam();
                if (!attackedTeam.equals(team)) {
                    player.teleport(team.getSpawn());
                    if (attackedTeam.getHealth() != 0 && attackedTeam.isCanDamaged()) {
                        Bukkit.getPluginManager().callEvent(new ArenaGoalEvent(arena, profile, attackedTeam));
                        if (attackedTeam.removeHealth())
                            Bukkit.getPluginManager().callEvent(new ArenaTeamLoseEvent(attackedTeam));
                        if (team.addHealth())
                            Bukkit.getPluginManager().callEvent(new ArenaTeamWinEvent(team));
                    }
                }
            }

        }
    }

    @EventHandler
    public void onDeathEntity(EntityDeathEvent e) {
        e.setDroppedExp(0);
        e.getDrops().clear();
    }

    @EventHandler
    public void onPortal(PlayerPortalEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        Player player = e.getPlayer();
        Profile profile = store.findProfile(player);
        if (profile != null && profile.isAlive() && profile.getArena() != null && profile.getArena().isGame()) {
            Bukkit.getPluginManager().callEvent(new ArenaBlockPlaceEvent(profile, e));
        } else e.setCancelled(true);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        Player player = e.getPlayer();
        Profile profile = store.findProfile(player);
        GameArena arena = profile.getArena();
        Block b = e.getBlock();
        if (profile != null && profile.isAlive() && arena != null && arena.isGame() &&
                (arena.getPlacedBlocks().contains(b) || fallings.contains(b))) {
            fallings.remove(b);
            Bukkit.getPluginManager().callEvent(new ArenaBlockBreakEvent(profile, e));
        } else e.setCancelled(true);
    }

    @EventHandler
    public void onExplode(EntityExplodeEvent e) {
        e.blockList().clear();
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        e.getRecipients().clear();

        Player player = e.getPlayer();
        Profile profile = store.findProfile(player);

        if (profile == null) {
            e.setCancelled(true);
            return;
        }

        GameArena arena = profile.getArena();
        GameTeam team = profile.getTeam();
        String msg = e.getMessage();
        String prefix;

        boolean isGlobal = msg.startsWith("!");

        if (arena.isGame()) {
            if (isGlobal) {
                prefix = "§7§l Всем §8| ";
                msg = msg.substring(1);
                for (Profile all : arena.getProfiles().map().values())
                    e.getRecipients().add(all.getPlayer());
            } else {
                prefix = " ";
                for (Profile all : team.getMembers())
                    e.getRecipients().add(all.getPlayer());
            }

        } else {
            prefix = " ";
            for (Profile all : arena.getProfiles().map().values())
                e.getRecipients().add(all.getPlayer());
        }

        e.setFormat(String.format("%s%s§8: §7%s", prefix, player.getDisplayName(), msg));
    }
}
