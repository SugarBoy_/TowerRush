package net.luckyverse.towerrush.arena;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.luckyverse.towerrush.TowerRush;
import net.luckyverse.towerrush.events.team.ArenaOreSpawnerEvent;
import net.luckyverse.towerrush.shops.upgrade.UpgradeSystemCore;
import net.luckyverse.towerrush.shops.upgrade.UpgradeType;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameOreSpawner implements Listener {

    private static final Random rnd = new Random();

    @Expose
    @SerializedName("location")
    public Location loc;

    @Expose
    @SerializedName("default")
    public boolean isActive = false;

    private GameTeam team;

    private List<BukkitTask> tasks = new ArrayList<>();

    public void init(GameTeam team) {
        this.team = team;
        this.loc = loc.add(0, 7, 0);
        team.getOreSpawners().add(this);
    }

    public void tick() {
        World w = loc.getWorld();
        UpgradeSystemCore upgrades = team.getArena().getUpgradeCore();
        if (rnd.nextInt(5) <= upgrades.getLevel(team, UpgradeType.ORE_SPEED) && w
                .getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ()).getType().equals(Material.AIR)) {

            spawn();
        }
    }

    public void activate() {
        isActive = true;
    }

    public boolean isActive() {
        return isActive;
    }

    public void spawn() {
        World w = loc.getWorld();
        for (double f = 0; f < 180 / Math.PI; f += 0.2) {
            w.spawnParticle(Particle.REDSTONE, loc.getX() - 0.5 * Math.cos(f), loc.getBlockY(),
                    loc.getZ() - 0.5 * Math.sin(f), 1, 0, 0, 0);
        }

        BukkitTask task = Bukkit.getScheduler().runTaskTimer(TowerRush.plugin(), new Runnable() {

            //double x = loc.getX();
            double y = loc.getBlockY();
            //double z = loc.getZ();
            float angle = (float) (180F / Math.PI * rnd.nextFloat());
            public void run() {

                w.spawnParticle(Particle.REDSTONE, loc.getX() + 0.5 * Math.cos(angle), y, loc.getZ() + 0.5 * Math.sin(angle), 1, 0, 0, 0);
                w.spawnParticle(Particle.REDSTONE, loc.getX() - 0.5 * Math.cos(angle), y, loc.getZ() - 0.5 * Math.sin(angle), 1, 0, 0, 0);
                angle += 0.1F;
                y -= 0.1F;

                if (y % 1 <= 0.5 && !w.getBlockAt(loc.getBlockX(), (int) y - 1, loc.getBlockZ())
                        .getType().equals(Material.AIR)) {

                    drawCube(w, loc.getBlockX(), (int) y, loc.getBlockZ());
                    tasks.remove(0).cancel();

                    Block b = w.getBlockAt(loc.getBlockX(), (int) y, loc.getBlockZ());
                    w.playSound(loc, Sound.BLOCK_STONE_PLACE, 1F, 0.3F);
                    Bukkit.getPluginManager().callEvent(new ArenaOreSpawnerEvent(team, GameOreSpawner.this, b));
                }
            }
        }, 0L, 1L);

        tasks.add(task);
        team.getArena().addTask(task);
    }

    private void drawCube(World w, int X, int Y, int Z) {
        Particle particle = Particle.SPELL_WITCH;

        for (float x = X; x < X + 1; x += 0.2F) {
            w.spawnParticle(particle, x, Y, Z, 1, 0, 0, 0);
            w.spawnParticle(particle, x, Y, Z + 1, 1, 0, 0, 0);
            w.spawnParticle(particle, x, Y + 1, Z + 1, 1, 0, 0, 0);
            w.spawnParticle(particle, x, Y + 1, Z, 1, 0, 0, 0);
        }

        for (float y = Y; y < Y + 1; y += 0.2F) {
            w.spawnParticle(particle, X, y, Z, 1, 0, 0, 0);
            w.spawnParticle(particle, X+1, y, Z, 1, 0, 0, 0);
            w.spawnParticle(particle, X, y, Z+1, 1, 0, 0, 0);
            w.spawnParticle(particle, X+1, y, Z+1, 1, 0, 0, 0);
        }

        for (float z = Z; z < Z + 1; z += 0.2F) {
            w.spawnParticle(particle, X, Y, z, 1, 0, 0, 0);
            w.spawnParticle(particle, X+1, Y, z, 1, 0, 0, 0);
            w.spawnParticle(particle, X, Y+1, z, 1, 0, 0, 0);
            w.spawnParticle(particle, X+1, Y+1, z, 1, 0, 0, 0);
        }
    }
}
