package net.luckyverse.towerrush.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandWorld implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("tr.admins")) {

                if (args.length == 1) {

                    String world = args[0];
                    World w = Bukkit.getWorld(world);

                    if (w == null) {
                        player.sendMessage("World not found!");
                        return true;
                    }

                    player.teleport(new Location(w, 0, 100, 0));
                    player.sendMessage("Teleporting..");

                } else {
                    player.sendMessage("usage: /world <worldname>");
                }

            }
        }
        return true;
    }
}
