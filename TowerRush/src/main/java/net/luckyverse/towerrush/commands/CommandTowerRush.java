package net.luckyverse.towerrush.commands;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameState;
import net.luckyverse.towerrush.profile.Profile;
import net.luckyverse.towerrush.stores.ArenaStore;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTowerRush implements CommandExecutor {

    private ArenaStore store;

    public CommandTowerRush(ArenaStore store) {
        this.store = store;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender.hasPermission("tr.admins")) {
            if (args.length != 0) {
                if (args.length == 1 && args[0].equalsIgnoreCase("info"))
                    for (GameArena arena : store.map().values()) {
                        sender.sendMessage(arena.toString());
                } else if (args.length == 1 && args[0].equalsIgnoreCase("start")) {
                    if (sender instanceof Player) {
                        Player player = (Player) sender;
                        GameArena arena = store.findProfile(player).getArena();
                        if (arena.isStarting()) {
                            if (arena.isWaiting())
                                arena.setState(GameState.STARTING);
                            arena.time = -1;
                            arena.setState(GameState.PLAYING);
                        }
                    }
                } else if (sender.hasPermission("tr.admins") && args.length == 1 && args[0].equalsIgnoreCase("test")) {
                    if (sender instanceof Player) {
                        Player player = (Player) sender;
                        Profile profile = store.findProfile(player);
                        GameArena arena = profile.getArena();
                        arena.getUpgradeCore().open(profile);
                    }
                }
            }
        }
        return true;
    }
}
