package net.luckyverse.towerrush.stores;

import net.luckyverse.sugarboy.CustomStore;
import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class ProfileStore extends CustomStore<Player, Profile> {

    public List<Profile> getNear(Location location, int radius) {
        return map.values().stream()
                .filter(p -> p.isAlive() && p.getPlayer().getWorld().equals(location.getWorld()) &&
                        p.getPlayer().getLocation().distanceSquared(location) <= radius)
                .collect(Collectors.toList());
    }

}
