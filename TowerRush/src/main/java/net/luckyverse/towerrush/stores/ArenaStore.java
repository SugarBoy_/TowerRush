package net.luckyverse.towerrush.stores;

import net.luckyverse.sugarboy.CustomStore;
import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.profile.Profile;
import net.luckyverse.towerrush.utils.configurations.ArenaSettings;
import org.bukkit.entity.Player;

public class ArenaStore extends CustomStore<String, GameArena> {

    public void initArena(ArenaSettings config, boolean copyScheme) {
        GameArena arena = new GameArena(config);
        add(config.world, arena);
        arena.load(copyScheme);
    }

    /*public GameArena findOptimal() {
        GameArena returnable = null;
        for (GameArena arena : map.values()) {
            if (arena.isStarting() && (returnable == null ||
                    (returnable.getProfiles().size() < arena.getProfiles().size() &&
                            arena.getProfiles().size() != arena.getLimit()))) {
                returnable = arena;
            }
        }
        return returnable;
    }*/

    public Profile findProfile(Player player) {
        GameArena arena = map.values().stream()
                .filter(a -> a.getProfiles().containsKey(player))
                .findFirst()
                .orElse(null);
        return arena == null ? null : arena.getProfiles().get(player);
    }

    public GameArena getByName(String arenaName) {
        for (GameArena arena : map.values()) {
            if (arena.ARENA_NAME.equals(arenaName))
                return arena;
        }
        return null;
    }
}
