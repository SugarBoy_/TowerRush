package net.luckyverse.towerrush.stores;

import net.luckyverse.sugarboy.CustomStore;
import net.luckyverse.towerrush.arena.GameTeam;
import net.luckyverse.towerrush.arena.listeners.GuiListener;
import net.luckyverse.towerrush.profile.Profile;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TeamStore extends CustomStore<Integer, GameTeam> {

    public void openInventorySelectTeam(Profile profile) {
        Inventory inv = Bukkit.createInventory(null, InventoryType.HOPPER, "§b§l » §3Выберите команду:");
        List<GameTeam> teams = map.values().stream().collect(Collectors.toList());
        Collections.sort(teams, Comparator.comparingInt(GameTeam::getCount));

        for (GameTeam team : teams) {
            ItemStack item = new ItemStack(Material.WOOL, 1, (short) team.getByteCode());
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(team.getColor() + team.getName());

            if (team.getCount() != 0) {
                List<String> lore = new ArrayList<>();
                lore.add(String.format("§7Игроки §7[§8%d§7/§8%d§7]:", team.getCount(), team.getLimit()));
                for (Profile member : team.getMembers())
                    lore.add(String.format(" §f» §7%s", member.getPlayer().getName()));
                meta.setLore(lore);
            }

            if (profile.getTeam() != null && profile.getTeam().equals(team)) {
                meta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 1, true);
                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            }

            item.setItemMeta(meta);
            inv.addItem(item);
        }
        GuiListener.addSelectTeam(inv);
        profile.getPlayer().openInventory(inv);
    }

    public void joinInRandom(Profile profile) {
        List<GameTeam> teams = map.values().stream().collect(Collectors.toList());
        Collections.sort(teams, Comparator.comparingInt(GameTeam::getCount));
        teams.get(0).join(profile);
    }
}
