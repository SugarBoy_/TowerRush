package net.luckyverse.towerrush;

import net.luckyverse.towerrush.arena.GameArena;
import net.luckyverse.towerrush.arena.GameState;
import net.luckyverse.towerrush.events.ArenaChangeStateEvent;
import net.luckyverse.towerrush.events.player.ArenaJoinEvent;
import net.luckyverse.towerrush.stores.ArenaStore;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import ru.yooxa.connecter.Core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CoreHandler implements Listener {

    private ArenaStore store;
    public CoreHandler(ArenaStore store) {
        this.store = store;
        Bukkit.getPluginManager().registerEvents(this, TowerRush.plugin());
        Bukkit.getScheduler().runTaskTimer(TowerRush.plugin(), ()-> sendInfo(), 300L, 200L);
    }

    public void sendInfo() {
        String info = "{\"server\":\"{SERVER}\",\"arenas\":{ARENAS}}"
                .replace("{SERVER}", TowerRush.plugin().getServerName())
                .replace("{ARENAS}", getArenasInfo().toString());

        sendToLobby("info", info);
    }

    public void updatePlayers(GameArena arena, int players) {
        sendToLobby("changeplayers", String.format("%s %s %d", TowerRush.plugin().getServerName(),
                arena.ARENA_NAME, players));
    }

    public void updateState(GameArena arena, GameState state) {
        sendToLobby("changestate", String.format("%s %s %d", TowerRush.plugin().getServerName(),
                arena.ARENA_NAME, state.number()));
    }

    private List<String> getArenasInfo() {
        List<String> arenas = new ArrayList<>();
        for (GameArena arena : store.map().values()) {

            int state = 3;
            if (arena.isGame())
                state = 2;
            else if (arena.isRestarting())
                state = 4;
            else if (arena.isStarting())
                state = arena.isWaiting() ? 0 : 1;

            String info = "{\"name\":\"{NAME}\",\"state\":{STATE},\"players\":{PLAYERS},\"limit\":{LIMIT},\"time\":{TIME}}"
                    .replace("{NAME}", arena.ARENA_NAME)
                    .replace("{STATE}", "" + state)
                    .replace("{PLAYERS}", "" + arena.getProfiles().size())
                    .replace("{LIMIT}", "" + arena.getLimit())
                    .replace("{TIME}", "" + arena.time);
            arenas.add(info);
        }
        return arenas;
    }

    private void sendToLobby(String tag, String msg) {
        try {
            Core.sendMessageToCore("KoT-Lobby-1", tag, msg);
            Core.sendMessageToCore("KoT-Lobby-2", tag, msg);
        } catch (IOException e) {
            TowerRush.logger().error(CoreHandler.class, "Ошибка соединия с CORE.");
        }
    }

    @EventHandler
    public void onJoin(ArenaJoinEvent e) {
        updatePlayers(e.getArena(), e.getArena().getProfiles().size());
    }

    @EventHandler
    public void onChangeState(ArenaChangeStateEvent e) {
        updateState(e.getArena(), e.getState());
    }
}
